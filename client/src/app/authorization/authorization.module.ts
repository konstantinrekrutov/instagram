import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {AuthorizationRoutingModule} from './authorization-routing.module';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {ConfirmComponent} from "./confirm/confirm.component";
import {PasswordResetComponent} from "./password-reset/password-reset.component";


@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        ConfirmComponent,
        PasswordResetComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        AuthorizationRoutingModule
    ]
})
export class AuthorizationModule {
}
