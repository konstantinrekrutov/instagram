import {Component, OnInit} from '@angular/core';
import {CategoryApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {Category} from "../../shared/sdk/models";
import {AuthService} from "../../services/auth/auth.service";

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
    categories: Array<Category> = [];

    constructor(private categoryApi: CategoryApi, private notify: NotifyService, private authService: AuthService) {
    }

    init() {
        this.categoryApi.find({where: {myUserId:this.authService.getUserId()}}).subscribe(data => {
            this.categories = <Array<Category>>data;
        }, error1 => {
            this.notify.notify(error1);
        })
    }

    ngOnInit() {
        this.init();
    }

    deleteCategory(category) {
        this.categoryApi.deleteById(category.id).subscribe(data => {
            this.init();
            this.notify.notify({code: 'Удалено'});
        }, error => {
            this.notify.notify({error: true, code: 'Ошибка'});
        })
    }

    saveCategory(category: Category) {
        this.categoryApi.updateAttributes(category.id, category).subscribe(data => {
            this.notify.notify({code: 'Сохранено!'});
        }, error1 => {
            this.notify.notify({error: true, code: 'Ошибка'});
        })
    }
}
