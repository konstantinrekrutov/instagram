import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {NotifyService} from "../../services/notify.service";
import {CategoryApi, InstagramApi, ReportApi, TaskApi} from "../../shared/sdk/services/custom";
import {Category, Task} from "../../shared/sdk/models";
import {LoopBackConfig} from "../../shared/sdk";
import {Subscription} from "rxjs/Subscription";

@Component({
    selector: 'app-task-edit',
    templateUrl: './task-edit.component.html',
    styleUrls: ['./task-edit.component.scss']
})
export class TaskEditComponent implements OnInit, OnDestroy {
    private taskId: any;
    task: Task;
    category: Category;
    accounts;
    instagrams;
    allInstagram;
    making:boolean = false;
    reports: any;
    sub: Subscription;
    serverHost= LoopBackConfig.getPath();
    private categoryId;
    makeReport() {
        let date;
        this.making = true;
        if(this.reports && this.reports.length) {
            date = new Date(this.reports[0].date);
        } else {
            date = new Date();
            date.setDate(date.getDate() - this.task.daysCountComments);
        }
        this.reportApi.makeReportComments(this.taskId, date.getTime()).subscribe(data => {
            this.making = false;
            this.reports.unshift(data);
            this.notify.notify(data);
        }, error => {
            this.making = false;
            this.notify.notify(error);
        });
    }

    constructor(private categoryApi: CategoryApi, private activeRoute: ActivatedRoute,
                private reportApi: ReportApi,
                private instagramApi: InstagramApi,
                private notify: NotifyService, private taskApi: TaskApi) {
    }

    ngOnInit() {
        this.sub = this.activeRoute.params.subscribe(data => {
            this.taskId = data['taskId'];
            this.categoryId = data['categoryId'];
            this.init();
        });

    }

    editTask() {
        this.taskApi.editTask({task: this.task, accounts:this.accounts, instagrams:this.instagrams})
            .subscribe(data => {
            this.notify.notify(data);
        }, error => {
            this.notify.notify(error);
        });
    }

    private init() {
        if (!this.taskId) {
            return this.notify.notify({error: true, code: 'Задача не найдена'})
        }

        this.instagramApi.find().subscribe(data => {
            this.allInstagram = data;
        });

        this.taskApi.findById(this.taskId, {include: ['accounts', 'instagrams']}).subscribe(task => {
            this.task = <Task>task;
            this.accounts = this.task['accounts'].map(item => {return item.username}).join("\n");

            this.reportApi.find({where: {taskId: this.task.id}, limit: 5, order:'id DESC'}).subscribe(data => {
                this.reports = data;
            }, error => {
                console.error(error);
            });

            this.instagrams = this.task.instagrams.map(function (instagram) {
                return instagram.id;
            });
        }, error => {
            this.notify.notify(error);
        })
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
}
