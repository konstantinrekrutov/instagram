import {Component, OnInit} from '@angular/core';
import {AccountApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn} from "@angular/forms";

@Component({
    selector: 'app-account-list',
    templateUrl: './account-list.component.html',
    styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {
    accounts = [];
    form: FormGroup;
    logins: string;
    loading: boolean = false;
    deleting: boolean = false;

    constructor(private accountApi: AccountApi, private notify: NotifyService, private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            accounts:  new FormArray([], this.minSelectedCheckboxes(1))
        });
    }
    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
            const totalSelected = formArray.controls
                .map(control => control.value)
                .reduce((prev, next) => next ? prev + next : prev, 0);
            return totalSelected >= min ? null : { required: true };
        };

        return validator;
    }

    addAccounts() {
        if (!this.logins) {
            return false;
        }
        this.loading = true;
        const logins = this.logins.split("\n").map(login => {
            return login.trim()
        });

        this.accountApi.addNew(logins).subscribe(data => {
            data.forEach((item) => {
                this.accounts.push(item);
            });
            this.addCheckboxes();
            this.notify.notify(data);
        }, error => {
            this.notify.notify(error);
        }, () => {
            this.loading = false;
        });
    }
    init(filter) {
        this.accountApi.find(filter).subscribe(data => {
            this.accounts = data;
            this.addCheckboxes();
        });
    }
    ngOnInit() {
        this.init({});
    }
    addCheckboxes() {
        this.accounts.map((o, i) => {
            if (i >= this.form.controls.accounts['length']) {
                const control = new FormControl(0);
                (this.form.controls.accounts as FormArray).push(control);
            }
        });
    }
    deleteAccounts() {
        const selected = this.form.value.accounts
            .map((v, i) => v ? this.accounts[i].id : null)
            .filter(v => v !== null);
        // @ts-ignore
        this.accountApi.removeAll({id: {inq: selected}}).subscribe(data => {
            this.form.controls.accounts = new FormArray([], this.minSelectedCheckboxes(1));
            this.accounts = this.accounts.filter(account => {
                return selected.indexOf(account.id) === -1;
            });
            this.addCheckboxes();
        }, error => {
            this.notify.notify(error);
        });
    }
}
