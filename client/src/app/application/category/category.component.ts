import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {CategoryApi} from "../../shared/sdk/services/custom";
import {Category} from "../../shared/sdk/models";
import {NotifyService} from "../../services/notify.service";
import {AuthService} from "../../services/auth/auth.service";

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    private categoryId: string;
    category: Category;
    accounts;
    constructor(private activeRoute: ActivatedRoute, private categoryApi:CategoryApi, private authService: AuthService,
                private notify:NotifyService) {
        this.category = new Category();
    }

    init() {
        this.categoryApi.findById(this.categoryId, {include: 'accounts'}).subscribe(data => {
            this.category = <Category>data;
            this.category.myUserId = this.authService.getUserId();
            this.accounts = this.category.accounts.map(item => {
                return item.username;
            }).join("\n");
        });
    }

    ngOnInit() {
        this.sub = this.activeRoute.params.subscribe(data => {
            this.categoryId = data['categoryId'];
            this.init()
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    saveCategory() {
        this.category.accounts = this.accounts;
        this.categoryApi.editCategory(this.category).subscribe(data => {
            this.notify.notify({code: 'saved'});
        }, error1 => {
            this.notify.notify({error: true, code: 'server_error'});
        })
    }
}
