import {Component, OnInit} from '@angular/core';
import {AccountApi, CategoryApi, InstagramApi, ParseUserTasksApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {Form, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {Category, ParseUserTasks} from "../../shared/sdk/models";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-parse-users',
    templateUrl: './parse-users.component.html',
    styleUrls: ['./parse-users.component.scss']
})
export class ParseUsersComponent implements OnInit {
    started: boolean;
    name: string;
    instagrams = [];
    tasks = [];
    accounts;
    form: FormGroup;
    task: ParseUserTasks;
    deleting: boolean = false;
    loading: boolean = false;
    private categoryId: any;
    private sub: Subscription;
    private category: Category;


    constructor(private accountApi: AccountApi, private taskApi: ParseUserTasksApi, private notify: NotifyService,
                private categoryApi: CategoryApi, private activatedRoute: ActivatedRoute,
                private notif: NotifyService, private instagramApi: InstagramApi, private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            tasks: new FormArray([], this.minSelectedCheckboxes(1))
        });
        this.task = new ParseUserTasks();
    }

    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
            const totalSelected = formArray.controls
                .map(control => control.value)
                .reduce((prev, next) => next ? prev + next : prev, 0);
            return totalSelected >= min ? null : {required: true};
        };

        return validator;
    }

    addCheckboxes() {
        this.tasks.map((o, i) => {
            if (i >= this.form.controls.tasks['length']) {
                const control = new FormControl(0);
                (this.form.controls.tasks as FormArray).push(control);
            }
        });
    }

    deleteTasks() {
        this.deleting = true;
        const selected = this.form.value.tasks
            .map((v, i) => v ? this.tasks[i].id : null)
            .filter(v => v !== null);
        // @ts-ignore
        this.taskApi.removeAll({id: {inq: selected}}).subscribe(data => {
            this.form.controls.tasks = new FormArray([], this.minSelectedCheckboxes(1));
            this.tasks = this.tasks.filter(task => {
                return selected.indexOf(task.id) === -1;
            });
            this.addCheckboxes();
            this.deleting = false;
        }, error => {
            this.deleting = false;
            this.notify.notify(error);
        });
    }

    init() {
        this.categoryApi.findById(this.categoryId, {include: 'accounts'}).subscribe(data => {
            console.log(data);
            this.category = <Category>data;
            this.task.categoryId = this.category.id;
            this.accounts = data['accounts'].map(item => {
                return item.username
            }).join("\n");
        });

        this.taskApi.find({where: {categoryId: this.categoryId}}).subscribe(data => {
            this.tasks = data;
            this.addCheckboxes();
        });

        this.instagramApi.find().subscribe(data => {
            this.instagrams = data;
        });
    }

    ngOnInit() {
        this.sub = this.activatedRoute.params.subscribe(data => {
            this.categoryId = data['categoryId'];
            this.init();
        });

    }

    addTask() {
        this.loading = true;
        this.task.accounts = this.accounts;
        this.taskApi.addNew(this.task).subscribe(data => {
            this.tasks.push(data);
            this.addCheckboxes();
            this.notify.notify(data);
            this.loading = false;
        }, err => {
            this.loading = false;
            this.notify.notify(err);
        })
    }

}
