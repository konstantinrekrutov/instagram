import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MeComponent} from "./me/me.component";
import {TaskListComponent} from "./task-list/task-list.component";
import {TaskEditComponent} from "./task-edit/task-edit.component";
import {ParseUsersComponent} from "./parse-users/parse-users.component";
import {ParseUsersEditComponent} from "./parse-users-edit/parse-users-edit.component";
import {InstagramAccountsComponent} from "./instagram-accounts/instagram-accounts.component";
import {CategoriesComponent} from "./categories/categories.component";
import {AddCategoryComponent} from "./add-category/add-category.component";
import {CategoryComponent} from "./category/category.component";

const routes: Routes = [ {
  path: '',
  component: MeComponent,
  pathMatch: 'full'
},{
    path: 'instagram-accounts',
    component: InstagramAccountsComponent
},{
    path: 'settings',
    component: CategoriesComponent
},{
    path: 'settings/add',
    component: AddCategoryComponent
},{
    path:'category/:categoryId',
    component: CategoryComponent
},
    {
    path: 'category/:categoryId/task-list',
    component: TaskListComponent
},{
    path: 'category/:categoryId/task-edit/:taskId',
    component: TaskEditComponent
},{
    path: 'category/:categoryId/parse-users',
    component: ParseUsersComponent
},{
    path: 'category/:categoryId/parse-users/:taskId',
    component: ParseUsersEditComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
