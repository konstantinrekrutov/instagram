import {Component, OnInit} from '@angular/core';
import {ReportApi} from "../../shared/sdk/services/custom";

@Component({
    selector: 'app-report-list',
    templateUrl: './report-list.component.html',
    styleUrls: ['./report-list.component.scss']
})
export class ReportListComponent implements OnInit {
    reports: any;

    constructor(private reportApi: ReportApi) {
    }

    ngOnInit() {
        this.reportApi.find().subscribe(data => {
            this.reports = data;
        }, error => {
            console.log(error);
        });
    }

    makeReport() {

    }
}
