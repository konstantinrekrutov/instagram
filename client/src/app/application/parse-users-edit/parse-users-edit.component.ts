import {Component, OnInit} from '@angular/core';
import {AccountApi, CategoryApi, InstagramApi, ParseUserTasksApi, ReportApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {ActivatedRoute} from "@angular/router";
import {Category, ParseUserTasks} from "../../shared/sdk/models";
import {LoopBackConfig} from "../../shared/sdk";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-parse-users-edit',
    templateUrl: './parse-users-edit.component.html',
    styleUrls: ['./parse-users-edit.component.scss']
})
export class ParseUsersEditComponent implements OnInit {
    accountsInWork = [];
    allInstagrams = [];
    accounts;
    instagrams = [];
    reports:any;
    making:boolean = false;
    serverHost= LoopBackConfig.getPath();
    task: ParseUserTasks = new ParseUserTasks();
    private categoryId: any;
    private sub: Subscription;
    private category: Category;
    constructor(private accountApi: AccountApi, private taskApi: ParseUserTasksApi, private activeRoute: ActivatedRoute,
                private categoryApi: CategoryApi, private activatedRoute: ActivatedRoute,
                private notify: NotifyService,  private reportApi: ReportApi,
                private instagramApi: InstagramApi) {
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(data => {
            this.task.id = data['taskId'];
            this.categoryId = data['categoryId'];
            this.init();
        })

    }

    init() {
        if (!this.task.id) {
            return this.notify.notify({error: true, code: 'Задача не найдена'})
        }

        this.categoryApi.findById(this.categoryId, {include: 'accounts'}).subscribe(data => {
            this.category = <Category>data;
        });

        this.reportApi.find({where: {taskId: this.task.id}, limit: 5, order:'id DESC'}).subscribe(data => {
            this.reports = data;
        }, error => {
            console.error(error);
        });
        this.instagramApi.find().subscribe(data => {
            this.allInstagrams = data;
        });

        this.taskApi.findById(this.task.id, {include: ['accounts', 'instagrams']}).subscribe(data => {
            this.task= <ParseUserTasks>data;
            this.accounts = this.task.accounts.map(item => {
                return item.username
            }).join("\n");
            this.instagrams = this.task.instagrams.map(instagram => {
                return instagram.id;
            });
        });
    }

    editTask() {
        this.taskApi.editTask({task: this.task, accounts: this.accounts, instagrams: this.instagrams}).subscribe(data => {
            this.notify.notify(data);
        }, err => {
            this.notify.notify(err);
        })
    }

    makeReport() {
        let date;
        this.making = true;
        if(this.reports && this.reports.length) {
            date = new Date(this.reports[0].date)
        } else {
            date = new Date(this.task.firstLapTime);
        }
        this.reportApi.makeReportFollowers(this.task.id, date.getTime()).subscribe(data => {
            this.reports.unshift(data);
            this.making = false;
            this.notify.notify(data);
        }, error => {
            this.making = false;
            this.notify.notify(error);
        });
    }
}
