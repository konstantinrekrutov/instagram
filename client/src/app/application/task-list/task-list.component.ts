import {Component, OnInit} from '@angular/core';
import {NotifyService} from "../../services/notify.service";
// @ts-ignore
import {AccountApi, CategoryApi, InstagramApi, TaskApi} from "../../shared/sdk/services/custom";
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {Category, Task} from "../../shared/sdk/models";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-task-list',
    templateUrl: './task-list.component.html',
    styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

    tasks = [];
    keywords = '';
    daysCountPost = 30;
    daysCountComments = 30;
    accounts;
    instagrams = [];
    loading: boolean;
    started: boolean;
    form: FormGroup;
    task: Task;
    deleting: boolean = false;
    private categoryId: string;
    private category: any;

    constructor(private accountApi: AccountApi, private taskApi: TaskApi, private categoryApi: CategoryApi,
                private activeRoute: ActivatedRoute,
                private notify: NotifyService, private instagramApi: InstagramApi, private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            tasks: new FormArray([], this.minSelectedCheckboxes(1))
        });
        this.task = new Task();
    }

    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
            const totalSelected = formArray.controls
                .map(control => control.value)
                .reduce((prev, next) => next ? prev + next : prev, 0);
            return totalSelected >= min ? null : {required: true};
        };

        return validator;
    }

    addTask() {
        this.loading = true;
        this.task.accounts = this.accounts;
        this.taskApi.addNew(this.task).subscribe(data => {
            this.tasks.push(data);
            this.addCheckboxes();
            this.loading = false;
            this.notify.notify(data);
        }, error => {
            this.loading = false;
            this.notify.notify(error);
        });
    }

    addCheckboxes() {
        this.tasks.map((o, i) => {
            if (i >= this.form.controls.tasks['length']) {
                const control = new FormControl(0);
                (this.form.controls.tasks as FormArray).push(control);
            }
        });
    }

    deleteTasks() {
        this.deleting = true;

        const selected = this.form.value.tasks
            .map((v, i) => v ? this.tasks[i].id : null)
            .filter(v => v !== null);
        // @ts-ignore
        this.taskApi.removeAll({id: {inq: selected}}).subscribe(data => {
            this.form.controls.tasks = new FormArray([], this.minSelectedCheckboxes(1));
            this.tasks = this.tasks.filter(task => {
                return selected.indexOf(task.id) === -1;
            });
            this.addCheckboxes();
            this.deleting = false;
        }, error => {
            this.deleting = false;
            this.notify.notify(error);
        });
    }

    init() {
        this.categoryApi.findById(this.categoryId, {include: 'accounts'}).subscribe(data => {
            this.category = <Category>data;
            this.task.daysCountPost = this.category.daysCountPost;
            this.task.daysCountComments = this.category.daysCountComments;
            this.task.keywords = this.category.keywords;
            this.task.minus = this.category.minus;
            this.task.categoryId = this.category.id;
            this.accounts = data['accounts'].map(item => {
                return item.username
            }).join("\n");
        });


        this.taskApi.find({where: {categoryId: this.categoryId}}).subscribe(data => {
            this.tasks = data;
            this.addCheckboxes();
        });
        this.instagramApi.find().subscribe(data => {
            this.instagrams = data;
        });

    }

    ngOnInit() {
        this.activeRoute.params.subscribe(data => {
            this.categoryId = data['categoryId'];
            this.init();
        });
    }

}
