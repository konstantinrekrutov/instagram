import {Component, OnInit} from '@angular/core';
import {CategoryApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {Category} from "../../shared/sdk/models";
import {AuthService} from "../../services/auth/auth.service";

@Component({
    selector: 'app-add-category',
    templateUrl: './add-category.component.html',
    styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
    name;
    category: Category;
    constructor(private categoryApi: CategoryApi, private notify: NotifyService, private authService: AuthService) {
        this.category = new Category();
        this.category.myUserId = authService.getUserId();
    }

    ngOnInit() {
    }

    addCategory() {
        if(!this.category.name) {
            return;
        }
        this.categoryApi.addNew(this.category).subscribe(data => {
            this.notify.notify({code: 'Добавлено!'});
        }, error1 => {
            this.notify.notify({error: true, code: 'Ошибка!'});
        })
    }
}
