import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotifyService} from "../../services/notify.service";
import {ControlsApi, StatApi} from "../../shared/sdk/services/custom";
import {Controls, Stat} from "../../shared/sdk/models";
import {AuthService} from "../../services/auth/auth.service";

@Component({
    selector: 'app-me',
    templateUrl: './me.component.html',
    styleUrls: ['./me.component.css']
})
export class MeComponent implements OnInit, OnDestroy {
    stat: Stat;
    controls: Controls;
    destroyed: boolean = false;
    constructor(private notify: NotifyService, private statApi: StatApi, private controlsApi: ControlsApi, public authService: AuthService) {

    }
    init() {
        if( this.destroyed ) return;
        this.controlsApi.findOne().subscribe(data => {
            this.controls = <Controls>data;
        }, error => {
            this.notify.notify(error);
        });
        this.statApi.findOne({include: {relation: 'logs', scope: {
                    skip:0,
                    limit:5,
                    order: 'id DESC'
                }}}).subscribe(data => {
            this.stat = <Stat>data;
            if(this.destroyed){
                return;
            }
            setTimeout(() => {
                this.init();
            }, 5000)
        }, error => {
            this.notify.notify(error);
        });
    }
    ngOnDestroy() {
        this.destroyed = true;
    }
    ngOnInit() {
        this.init();
    }


    stopAll() {
        this.controlsApi.updateAttributes(this.controls.id, {stopped: true}).subscribe(data => {
            this.notify.notify(data);
        }, error => {
            this.notify.notify(error);
        })
    }

    startAll() {
        this.controlsApi.updateAttributes(this.controls.id, {stopped: false}).subscribe(data => {
            this.notify.notify(data);
        }, error => {
            this.notify.notify(error);
        })
    }
}
