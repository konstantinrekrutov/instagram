import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { CommonModule } from '@angular/common';
import { ApplicationRoutingModule } from './application-routing.module';
import {MeComponent} from "./me/me.component";
import { TaskListComponent } from './task-list/task-list.component';
import { AccountListComponent } from './account-list/account-list.component';
import { TaskEditComponent } from './task-edit/task-edit.component';
import { ParseUsersComponent } from './parse-users/parse-users.component';
import { ParseUsersEditComponent } from './parse-users-edit/parse-users-edit.component';
import { ReportListComponent } from './report-list/report-list.component';
import { InstagramAccountsComponent } from './instagram-accounts/instagram-accounts.component';
import { CategoriesComponent } from './categories/categories.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoryComponent } from './category/category.component';

@NgModule({
  declarations: [
    MeComponent,
    TaskListComponent,
    AccountListComponent,
    TaskEditComponent,
    ParseUsersComponent,
    ParseUsersEditComponent,
    ReportListComponent,
    InstagramAccountsComponent,
    CategoriesComponent,
    AddCategoryComponent,
    CategoryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ApplicationRoutingModule,
    ReactiveFormsModule
  ]
})
export class ApplicationModule { }
