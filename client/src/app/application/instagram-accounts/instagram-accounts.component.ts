import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {InstagramApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {Instagram} from "../../shared/sdk/models";

@Component({
    selector: 'app-instagram-accounts',
    templateUrl: './instagram-accounts.component.html',
    styleUrls: ['./instagram-accounts.component.scss']
})
export class InstagramAccountsComponent implements OnInit {
    login: string;
    password: string;
    accounts;
    form: FormGroup;
    deleting: boolean = false;

    constructor(private instagramApi: InstagramApi, private notify: NotifyService, private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            accounts: new FormArray([], this.minSelectedCheckboxes(1))
        });
    }

    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
            const totalSelected = formArray.controls
                .map(control => control.value)
                .reduce((prev, next) => next ? prev + next : prev, 0);
            return totalSelected >= min ? null : {required: true};
        };

        return validator;
    }

    ngOnInit() {
        this.getInst();
    }

    auth(acc: Instagram) {
        this.instagramApi.authorize(acc.id).subscribe(data => {
            this.notify.notify(data);
        }, err => {
            this.notify.notify(err);
        });
    }

    getInst() {
        this.instagramApi.find().subscribe(data => {
            this.accounts = data;
            this.addCheckboxes();
        })
    }

    addInst() {
        this.instagramApi.addAccount(this.login, this.password).subscribe(data => {
            this.accounts.push(data);
            this.addCheckboxes();
            this.notify.notify(data);
        }, error => {
            this.notify.notify(error);
        })
    }

    addCheckboxes() {
        this.accounts.map((o, i) => {
            if (i >= this.form.controls.accounts['length']) {
                const control = new FormControl(0);
                (this.form.controls.accounts as FormArray).push(control);
            }
        });
    }

    deleteAccounts() {
        this.deleting = true;
        const selected = this.form.value.accounts
            .map((v, i) => v ? this.accounts[i].id : null)
            .filter(v => v !== null);
        // @ts-ignore
        this.instagramApi.removeAll({id: {inq: selected}}).subscribe(data => {
            this.form.controls.accounts = new FormArray([], this.minSelectedCheckboxes(1));
            this.accounts = this.accounts.filter(account => {
                return selected.indexOf(account.id) === -1;
            });
            this.deleting = false;
            this.addCheckboxes();
        }, error => {
            this.deleting = false;
            this.notify.notify(error);
        });
    }
}
