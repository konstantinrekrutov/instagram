import {Component, OnInit} from '@angular/core';
import {MyUserApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {MyUser} from "../../shared/sdk/models";
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn} from "@angular/forms";

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    users: Array<MyUser> = [];
    form: FormGroup;
    user: MyUser;
    passwordConfirm;
    loading;
    deleting: boolean;

    constructor(private myUserApi: MyUserApi, private notify: NotifyService, private formBuilder: FormBuilder) {
        this.user = new MyUser();
        this.form = this.formBuilder.group({
            users: new FormArray([], this.minSelectedCheckboxes(1))
        });
    }

    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
            const totalSelected = formArray.controls
                .map(control => control.value)
                .reduce((prev, next) => next ? prev + next : prev, 0);
            return totalSelected >= min ? null : {required: true};
        };

        return validator;
    }

    ngOnInit() {
        this.myUserApi.find().subscribe(data => {
            this.users = <Array<MyUser>>data;
            this.addCheckboxes();

        });
    }

    addCheckboxes() {
        this.users.map((o, i) => {
            if (i >= this.form.controls.users['length']) {
                const control = new FormControl(0);
                (this.form.controls.users as FormArray).push(control);
            }
        });
    }

    addUser() {
        if (this.passwordConfirm !== this.user.password) {
            this.notify.notify({error: true, code: 'Неверное подтверждение пароля'});
            return;
        }
        this.user.emailVerified = true;
        this.myUserApi.create(this.user).subscribe(data => {
            this.users.push(<MyUser>data);
            this.addCheckboxes();
            this.notify.notify({code: 'user_added'});
        }, error => {
            this.notify.notify({error: true, code: 'user_not_added'})
        })
    }

    deleteUsers() {
        this.deleting = true;

        const selected = this.form.value.users
            .map((v, i) => v ? this.users[i].id : null)
            .filter(v => v !== null);
        // @ts-ignore
        this.myUserApi.removeAll({id: {inq: selected}}).subscribe(data => {
            this.form.controls.users = new FormArray([], this.minSelectedCheckboxes(1));
            this.users = this.users.filter(task => {
                return selected.indexOf(task.id) === -1;
            });
            this.addCheckboxes();
            this.deleting = false;
        }, error => {
            this.deleting = false;
            this.notify.notify(error);
        });
    }
}
