import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkTasksComponent } from './link-tasks.component';

describe('LinkTasksComponent', () => {
  let component: LinkTasksComponent;
  let fixture: ComponentFixture<LinkTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
