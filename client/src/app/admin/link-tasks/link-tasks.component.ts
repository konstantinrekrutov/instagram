import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {CategoryApi, MyUserApi, TaskApi} from "../../shared/sdk/services/custom";
import {Category, MyUser, Task} from "../../shared/sdk/models";
import {NotifyService} from "../../services/notify.service";

@Component({
    selector: 'app-link-tasks',
    templateUrl: './link-tasks.component.html',
    styleUrls: ['./link-tasks.component.scss']
})
export class LinkTasksComponent implements OnInit {
    tasks: Array<Task> = [];
    categories: Array<Category> = [];
    users: Array<MyUser> = [];
    categoryUserMap = {};
    un = {};
    editingTask: Task;
    editingCategory: Category;
    userId: any;
    categoryId: any;
    userEditing: boolean;
    categoryEditing: boolean;
    @ViewChild('userList') userListEl:ElementRef;
    @ViewChild('categoryList') categoryListEl:ElementRef;
    private oldCategoryId: string;
    private oldUserId: string;
    constructor(private tasksApi: TaskApi, private categoryApi: CategoryApi, private renderer: Renderer2,
                private notify: NotifyService, private myUserApi: MyUserApi) {
    }

    ngOnInit() {
        this.tasksApi.find({include: 'category'}).subscribe(tasks => {
            this.tasks = <Array<Task>>tasks;
        });
        this.categoryApi.find({include: 'myUser'}).subscribe(categories => {
            this.categories = <Array<Category>>categories;
            this.categories.forEach((category) => {
                if (category.myUser && !this.categoryUserMap[category.id]) {
                    this.categoryUserMap[category.id] = category.myUser;
                }
            })
        });
        this.myUserApi.find({include: 'categories'}).subscribe(users => {
            this.users = <Array<MyUser>>users;
            this.users.forEach((user) => {
                user.categories.forEach((category) => {
                    if (!this.categoryUserMap[category.id]) {
                        this.categoryUserMap[category.id] = user;
                    }

                })
            })
        });
    }

    getUser (task: Task) {
        const category = task.category;
        const user = this.categoryUserMap[category.id];
        if (user) {
            return user;
        } else {
            return new MyUser();
        }
    }

    editTaskUser($event, task: Task, oldUserId) {
        this.userId = null;
        this.categoryId = null;
        this.editingTask = task;
        this.oldUserId = oldUserId;

        const target = $event.target || $event.srcElement || $event.currentTarget;
        const coords = target.parentNode.getBoundingClientRect();
        this.renderer.setStyle(this.userListEl.nativeElement, 'left', coords['left'] + 10.5 + 'px');
        this.renderer.setStyle(this.userListEl.nativeElement, 'top', coords['top'] + 5 + 'px');
        this.userEditing = true;
        this.categoryEditing = false;
    }

    editTaskCategory($event, task: Task, oldCategoryId) {
        this.userId = null;
        this.categoryId = null;
        this.editingTask = task;
        this.oldCategoryId = oldCategoryId;
        const target = $event.target || $event.srcElement || $event.currentTarget;
        const coords = target.parentNode.getBoundingClientRect();
        this.renderer.setStyle(this.categoryListEl.nativeElement, 'left', coords['left'] + 10.5 + 'px');
        this.renderer.setStyle(this.categoryListEl.nativeElement, 'top', coords['top'] + 5 + 'px');
        this.categoryEditing = true;
        this.userEditing = false;

    }
    editCategory($event, category: Category, oldUser) {
        this.editingTask = null;
        this.userId = null;
        this.categoryId = null;
        this.editingCategory = category;
        this.oldUserId = oldUser ? oldUser.id : null;
        const target = $event.target || $event.srcElement || $event.currentTarget;
        const coords = target.parentNode.getBoundingClientRect();
        this.renderer.setStyle(this.userListEl.nativeElement, 'left', coords['left'] + 10.5 + 'px');
        this.renderer.setStyle(this.userListEl.nativeElement, 'top', coords['top'] + 5 + 'px');
        this.categoryEditing = false;
        this.userEditing = true;
    }
    setUser() {
        this.userEditing = true;
        this.categoryEditing = false;
        let category;
        if(!this.editingTask) {
            category = this.editingCategory;
        } else {
            if(!this.editingTask.categoryId) {
                return;
            }
            category = this.categories.find((item) => {
                return item.id === this.editingTask.categoryId;
            });
        }


        const user = this.users.find((item) => {
            return item.id === this.userId;
        });
        if(!category || !user) {
            return;
        }

        this.categoryApi.setUser(category.id, user.id).subscribe(data => {
            this.notify.notify({code: 'Сохранено'});
        }, error1 => {
            this.notify.notify({code: 'Ошибка'});
        })
    }

    setCategory() {
        this.categoryEditing = true;
        this.userEditing = false;

        const category = this.categories.find((item) => {
            return item.id === this.categoryId;
        });
        if(!category) {
            return;
        }
        this.editingTask.category = category;
        this.editingTask.categoryId = this.categoryId;
        this.tasksApi.updateAttributes(this.editingTask.id, this.editingTask).subscribe(data => {
            this.notify.notify({code: 'Сохранено'});
        }, error1 => {
            this.notify.notify({code: 'Ошибка'});
        })

    }



}
