import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from "./user-list/user-list.component";
import {EditUserComponent} from "./edit-user/edit-user.component";
import {LinkTasksComponent} from "./link-tasks/link-tasks.component";

const routes: Routes = [{
    path: '',
    component: UserListComponent,
    pathMatch: 'full'
},{
    path: 'edit-user/:id',
    component: EditUserComponent
},{
    path: 'link-tasks',
    component: LinkTasksComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
