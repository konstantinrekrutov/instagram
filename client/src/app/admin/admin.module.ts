import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { EditUserComponent } from './edit-user/edit-user.component';
import { LinkTasksComponent } from './link-tasks/link-tasks.component';

@NgModule({
  declarations: [UserListComponent,
      EditUserComponent,
      LinkTasksComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
      FormsModule,
      ReactiveFormsModule
  ]
})
export class AdminModule { }
