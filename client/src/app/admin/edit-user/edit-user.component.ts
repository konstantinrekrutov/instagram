import { Component, OnInit } from '@angular/core';
import {MyUser} from "../../shared/sdk/models";
import {MyUserApi} from "../../shared/sdk/services/custom";
import {NotifyService} from "../../services/notify.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  passwordConfirm: any;
  user: MyUser = new MyUser();
  loading: boolean;


  constructor(private myUserApi: MyUserApi, private notify: NotifyService, private route: ActivatedRoute) {

  }
  init() {
    this.myUserApi.findById(this.user.id).subscribe(user => {
      this.user = <MyUser>user;
      this.user.emailVerified = true;
    })
  }
  ngOnInit() {
    this.route.params.subscribe(data => {
      this.user.id = data['id'];
      this.init();
    })
  }

  editUser() {
    this.myUserApi.updateUser(this.user).subscribe(user => {
      this.notify.notify({code:"Обновлён"})
    }, error1 => {
      this.notify.notify(error1);
    })
  }
}
