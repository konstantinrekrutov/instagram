import {Component, isDevMode} from '@angular/core';
import {LoopBackConfig} from "./shared/sdk";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'client';

    constructor() {
        const server = isDevMode() ? 'http://127.0.0.1:9089' : 'http://31.130.206.73:9089';
        LoopBackConfig.setBaseURL(server);
        LoopBackConfig.setApiVersion('api');
    }
}
