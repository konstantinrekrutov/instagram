import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MyUserApi} from "../../shared/sdk/services/custom";
import {Observable} from "rxjs/index";
import {CacheService} from "../cache.service";

@Injectable()
export class AuthService {
    constructor(private http: HttpClient, private myUserApi: MyUserApi, private cache: CacheService) {
    }

    public isAuthenticated(): boolean {
        return this.myUserApi.isAuthenticated();
    }

    public isAdmin() {
        return new Observable<boolean>(observer => {
            this.getUserRoles().subscribe(data => {
                observer.next(data.indexOf('admin') !== -1);
                observer.complete();
            })
        })
    }

    public getUserRoles() {
        const key = this.myUserApi.getCurrentId() + 'roles';
        return new Observable<any>(observer => {
            if (this.cache.has(key)) {
                this.cache.get(key).subscribe(data => {
                    observer.next(data);
                    observer.complete();

                })
            } else {
                this.myUserApi.getMyRole().subscribe(data => {
                    this.cache.set(key, data);
                    observer.next(data);
                    observer.complete();
                });
            }

        });
    }
    public getUserId() {
        return this.myUserApi.getCurrentId();
    }

    public resetPassword(email) {
        return this.myUserApi.resetPasswordSend(email);
    }

    public changePassword(options) {
        return this.myUserApi.resetMyPassword(options);

    }

    public login(options) {
        return this.myUserApi.login(options);
    }

    logout() {
       this.myUserApi.logout();
    }
}
