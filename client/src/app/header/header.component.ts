import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {CategoryApi} from "../shared/sdk/services/custom";
import {Category} from "../shared/sdk/models";
import {AuthService} from "../services/auth/auth.service";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    currentUrl: string;
    isAdmin: boolean = false;
    categories: Array<Category> = [];

    constructor(private router: Router, private categoryApi: CategoryApi, private authService: AuthService) {

    }
    init() {
        this.authService.isAdmin().subscribe(isAdmin => {
            this.isAdmin = isAdmin;
        });
        this.categoryApi.find({where: {myUserId: this.authService.getUserId()}}).subscribe(data => {
            this.categories = <Array<Category>>data;

        })
    }

    ngOnInit() {
        this.sub = this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.currentUrl = event.url;
            }
        });
        this.init();
    }

    logout() {
        this.authService.logout();
        this.router.navigate(['/auth/login']);
    }
    ngOnDestroy() {
        this.sub.unsubscribe();
    }

}
