/* tslint:disable */
/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root 
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
import { ErrorHandler } from './services/core/error.service';
import { LoopBackAuth } from './services/core/auth.service';
import { LoggerService } from './services/custom/logger.service';
import { SDKModels } from './services/custom/SDKModels';
import { InternalStorage, SDKStorage } from './storage/storage.swaps';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CookieBrowser } from './storage/cookie.browser';
import { StorageBrowser } from './storage/storage.browser';
import { SocketBrowser } from './sockets/socket.browser';
import { SocketDriver } from './sockets/socket.driver';
import { SocketConnection } from './sockets/socket.connections';
import { RealTime } from './services/core/real.time';
import { EmailApi } from './services/custom/Email';
import { MyUserApi } from './services/custom/MyUser';
import { MyAccessTokenApi } from './services/custom/MyAccessToken';
import { InstagramApi } from './services/custom/Instagram';
import { TaskApi } from './services/custom/Task';
import { Task_to_accountApi } from './services/custom/Task_to_account';
import { Task_to_instagramApi } from './services/custom/Task_to_instagram';
import { PostsApi } from './services/custom/Posts';
import { CommentsApi } from './services/custom/Comments';
import { HistoryFromApi } from './services/custom/HistoryFrom';
import { PaginationApi } from './services/custom/Pagination';
import { ParseUserTasksApi } from './services/custom/ParseUserTasks';
import { Parseuser_to_accountsApi } from './services/custom/Parseuser_to_accounts';
import { Parseuser_to_instagramApi } from './services/custom/Parseuser_to_instagram';
import { FollowersApi } from './services/custom/Followers';
import { Followers_to_accountsApi } from './services/custom/Followers_to_accounts';
import { ReportApi } from './services/custom/Report';
import { AccountApi } from './services/custom/Account';
import { StatApi } from './services/custom/Stat';
import { LogApi } from './services/custom/Log';
import { ControlsApi } from './services/custom/Controls';
import { CategoryApi } from './services/custom/Category';
import { Category_accountApi } from './services/custom/Category_account';
import { CommentCursorApi } from './services/custom/CommentCursor';
/**
* @module SDKBrowserModule
* @description
* This module should be imported when building a Web Application in the following scenarios:
*
*  1.- Regular web application
*  2.- Angular universal application (Browser Portion)
*  3.- Progressive applications (Angular Mobile, Ionic, WebViews, etc)
**/
@NgModule({
  imports:      [ CommonModule, HttpClientModule ],
  declarations: [ ],
  exports:      [ ],
  providers:    [
    ErrorHandler,
    SocketConnection
  ]
})
export class SDKBrowserModule {
  static forRoot(internalStorageProvider: any = {
    provide: InternalStorage,
    useClass: CookieBrowser
  }): ModuleWithProviders {
    return {
      ngModule  : SDKBrowserModule,
      providers : [
        LoopBackAuth,
        LoggerService,
        SDKModels,
        RealTime,
        EmailApi,
        MyUserApi,
        MyAccessTokenApi,
        InstagramApi,
        TaskApi,
        Task_to_accountApi,
        Task_to_instagramApi,
        PostsApi,
        CommentsApi,
        HistoryFromApi,
        PaginationApi,
        ParseUserTasksApi,
        Parseuser_to_accountsApi,
        Parseuser_to_instagramApi,
        FollowersApi,
        Followers_to_accountsApi,
        ReportApi,
        AccountApi,
        StatApi,
        LogApi,
        ControlsApi,
        CategoryApi,
        Category_accountApi,
        CommentCursorApi,
        internalStorageProvider,
        { provide: SDKStorage, useClass: StorageBrowser },
        { provide: SocketDriver, useClass: SocketBrowser }
      ]
    };
  }
}
/**
* Have Fun!!!
* - Jon
**/
export * from './models/index';
export * from './services/index';
export * from './lb.config';
export * from './storage/storage.swaps';
export { CookieBrowser } from './storage/cookie.browser';
export { StorageBrowser } from './storage/storage.browser';

