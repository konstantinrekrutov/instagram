/* tslint:disable */
import { Injectable } from '@angular/core';
import { Email } from '../../models/Email';
import { MyUser } from '../../models/MyUser';
import { MyAccessToken } from '../../models/MyAccessToken';
import { Instagram } from '../../models/Instagram';
import { Task } from '../../models/Task';
import { Task_to_account } from '../../models/Task_to_account';
import { Task_to_instagram } from '../../models/Task_to_instagram';
import { Posts } from '../../models/Posts';
import { Comments } from '../../models/Comments';
import { HistoryFrom } from '../../models/HistoryFrom';
import { Pagination } from '../../models/Pagination';
import { ParseUserTasks } from '../../models/ParseUserTasks';
import { Parseuser_to_accounts } from '../../models/Parseuser_to_accounts';
import { Parseuser_to_instagram } from '../../models/Parseuser_to_instagram';
import { Followers } from '../../models/Followers';
import { Followers_to_accounts } from '../../models/Followers_to_accounts';
import { Report } from '../../models/Report';
import { Account } from '../../models/Account';
import { Stat } from '../../models/Stat';
import { Log } from '../../models/Log';
import { Controls } from '../../models/Controls';
import { Category } from '../../models/Category';
import { Category_account } from '../../models/Category_account';
import { CommentCursor } from '../../models/CommentCursor';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Email: Email,
    MyUser: MyUser,
    MyAccessToken: MyAccessToken,
    Instagram: Instagram,
    Task: Task,
    Task_to_account: Task_to_account,
    Task_to_instagram: Task_to_instagram,
    Posts: Posts,
    Comments: Comments,
    HistoryFrom: HistoryFrom,
    Pagination: Pagination,
    ParseUserTasks: ParseUserTasks,
    Parseuser_to_accounts: Parseuser_to_accounts,
    Parseuser_to_instagram: Parseuser_to_instagram,
    Followers: Followers,
    Followers_to_accounts: Followers_to_accounts,
    Report: Report,
    Account: Account,
    Stat: Stat,
    Log: Log,
    Controls: Controls,
    Category: Category,
    Category_account: Category_account,
    CommentCursor: CommentCursor,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
