/* tslint:disable */
import {
  Task,
  Posts,
  Followers,
  HistoryFrom
} from '../index';

declare var Object: any;
export interface AccountInterface {
  "username": string;
  "id"?: string;
  tasks?: Task[];
  posts?: Posts[];
  followers?: Followers[];
  historyFroms?: HistoryFrom;
}

export class Account implements AccountInterface {
  "username": string;
  "id": string;
  tasks: Task[];
  posts: Posts[];
  followers: Followers[];
  historyFroms: HistoryFrom;
  constructor(data?: AccountInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Account`.
   */
  public static getModelName() {
    return "Account";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Account for dynamic purposes.
  **/
  public static factory(data: AccountInterface): Account{
    return new Account(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Account',
      plural: 'Accounts',
      path: 'Accounts',
      idName: 'id',
      properties: {
        "username": {
          name: 'username',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
      },
      relations: {
        tasks: {
          name: 'tasks',
          type: 'Task[]',
          model: 'Task',
          relationType: 'hasMany',
          modelThrough: 'Task_to_account',
          keyThrough: 'taskId',
          keyFrom: 'id',
          keyTo: 'accountId'
        },
        posts: {
          name: 'posts',
          type: 'Posts[]',
          model: 'Posts',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'accountId'
        },
        followers: {
          name: 'followers',
          type: 'Followers[]',
          model: 'Followers',
          relationType: 'hasMany',
          modelThrough: 'Followers_to_accounts',
          keyThrough: 'followerId',
          keyFrom: 'id',
          keyTo: 'accountId'
        },
        historyFroms: {
          name: 'historyFroms',
          type: 'HistoryFrom',
          model: 'HistoryFrom',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'accountId'
        },
      }
    }
  }
}
