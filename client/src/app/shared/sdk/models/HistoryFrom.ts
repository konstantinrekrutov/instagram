/* tslint:disable */
import {
  Account
} from '../index';

declare var Object: any;
export interface HistoryFromInterface {
  "time": number;
  "id"?: any;
  "accountId"?: string;
  account?: Account;
}

export class HistoryFrom implements HistoryFromInterface {
  "time": number;
  "id": any;
  "accountId": string;
  account: Account;
  constructor(data?: HistoryFromInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `HistoryFrom`.
   */
  public static getModelName() {
    return "HistoryFrom";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of HistoryFrom for dynamic purposes.
  **/
  public static factory(data: HistoryFromInterface): HistoryFrom{
    return new HistoryFrom(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'HistoryFrom',
      plural: 'HistoryFroms',
      path: 'HistoryFroms',
      idName: 'id',
      properties: {
        "time": {
          name: 'time',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "accountId": {
          name: 'accountId',
          type: 'string'
        },
      },
      relations: {
        account: {
          name: 'account',
          type: 'Account',
          model: 'Account',
          relationType: 'belongsTo',
                  keyFrom: 'accountId',
          keyTo: 'id'
        },
      }
    }
  }
}
