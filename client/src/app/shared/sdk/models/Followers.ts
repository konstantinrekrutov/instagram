/* tslint:disable */
import {
  Account
} from '../index';

declare var Object: any;
export interface FollowersInterface {
  "id"?: number;
  "username": string;
  "createdAt": Date;
  accounts?: Account[];
}

export class Followers implements FollowersInterface {
  "id": number;
  "username": string;
  "createdAt": Date;
  accounts: Account[];
  constructor(data?: FollowersInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Followers`.
   */
  public static getModelName() {
    return "Followers";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Followers for dynamic purposes.
  **/
  public static factory(data: FollowersInterface): Followers{
    return new Followers(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Followers',
      plural: 'Followers',
      path: 'Followers',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
      },
      relations: {
        accounts: {
          name: 'accounts',
          type: 'Account[]',
          model: 'Account',
          relationType: 'hasMany',
          modelThrough: 'Followers_to_accounts',
          keyThrough: 'accountId',
          keyFrom: 'id',
          keyTo: 'followerId'
        },
      }
    }
  }
}
