/* tslint:disable */
import {
  ParseUserTasks,
  Instagram
} from '../index';

declare var Object: any;
export interface Parseuser_to_instagramInterface {
  "id"?: any;
  "taskId"?: any;
  "instagramId"?: any;
  task?: ParseUserTasks;
  instagram?: Instagram;
}

export class Parseuser_to_instagram implements Parseuser_to_instagramInterface {
  "id": any;
  "taskId": any;
  "instagramId": any;
  task: ParseUserTasks;
  instagram: Instagram;
  constructor(data?: Parseuser_to_instagramInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Parseuser_to_instagram`.
   */
  public static getModelName() {
    return "Parseuser_to_instagram";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Parseuser_to_instagram for dynamic purposes.
  **/
  public static factory(data: Parseuser_to_instagramInterface): Parseuser_to_instagram{
    return new Parseuser_to_instagram(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Parseuser_to_instagram',
      plural: 'Parseuser_to_instagrams',
      path: 'Parseuser_to_instagrams',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "taskId": {
          name: 'taskId',
          type: 'any'
        },
        "instagramId": {
          name: 'instagramId',
          type: 'any'
        },
      },
      relations: {
        task: {
          name: 'task',
          type: 'ParseUserTasks',
          model: 'ParseUserTasks',
          relationType: 'belongsTo',
                  keyFrom: 'taskId',
          keyTo: 'id'
        },
        instagram: {
          name: 'instagram',
          type: 'Instagram',
          model: 'Instagram',
          relationType: 'belongsTo',
                  keyFrom: 'instagramId',
          keyTo: 'id'
        },
      }
    }
  }
}
