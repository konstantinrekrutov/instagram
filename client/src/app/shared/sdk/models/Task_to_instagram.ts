/* tslint:disable */
import {
  Instagram,
  Task
} from '../index';

declare var Object: any;
export interface Task_to_instagramInterface {
  "instagramId": any;
  "taskId": any;
  "id"?: any;
  instagram?: Instagram;
  task?: Task;
}

export class Task_to_instagram implements Task_to_instagramInterface {
  "instagramId": any;
  "taskId": any;
  "id": any;
  instagram: Instagram;
  task: Task;
  constructor(data?: Task_to_instagramInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Task_to_instagram`.
   */
  public static getModelName() {
    return "Task_to_instagram";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Task_to_instagram for dynamic purposes.
  **/
  public static factory(data: Task_to_instagramInterface): Task_to_instagram{
    return new Task_to_instagram(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Task_to_instagram',
      plural: 'Task_to_instagrams',
      path: 'Task_to_instagrams',
      idName: 'id',
      properties: {
        "instagramId": {
          name: 'instagramId',
          type: 'any'
        },
        "taskId": {
          name: 'taskId',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        instagram: {
          name: 'instagram',
          type: 'Instagram',
          model: 'Instagram',
          relationType: 'belongsTo',
                  keyFrom: 'instagramId',
          keyTo: 'id'
        },
        task: {
          name: 'task',
          type: 'Task',
          model: 'Task',
          relationType: 'belongsTo',
                  keyFrom: 'taskId',
          keyTo: 'id'
        },
      }
    }
  }
}
