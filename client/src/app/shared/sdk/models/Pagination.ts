/* tslint:disable */
import {
  Posts
} from '../index';

declare var Object: any;
export interface PaginationInterface {
  "after": string;
  "id"?: any;
  "postsId"?: any;
  posts?: Posts;
}

export class Pagination implements PaginationInterface {
  "after": string;
  "id": any;
  "postsId": any;
  posts: Posts;
  constructor(data?: PaginationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Pagination`.
   */
  public static getModelName() {
    return "Pagination";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Pagination for dynamic purposes.
  **/
  public static factory(data: PaginationInterface): Pagination{
    return new Pagination(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Pagination',
      plural: 'Paginations',
      path: 'Paginations',
      idName: 'id',
      properties: {
        "after": {
          name: 'after',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "postsId": {
          name: 'postsId',
          type: 'any'
        },
      },
      relations: {
        posts: {
          name: 'posts',
          type: 'Posts',
          model: 'Posts',
          relationType: 'belongsTo',
                  keyFrom: 'postsId',
          keyTo: 'id'
        },
      }
    }
  }
}
