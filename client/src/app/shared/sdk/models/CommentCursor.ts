/* tslint:disable */

declare var Object: any;
export interface CommentCursorInterface {
  "postId": string;
  "type": string;
  "cursor": string;
  "id"?: any;
}

export class CommentCursor implements CommentCursorInterface {
  "postId": string;
  "type": string;
  "cursor": string;
  "id": any;
  constructor(data?: CommentCursorInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `CommentCursor`.
   */
  public static getModelName() {
    return "CommentCursor";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of CommentCursor for dynamic purposes.
  **/
  public static factory(data: CommentCursorInterface): CommentCursor{
    return new CommentCursor(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'CommentCursor',
      plural: 'CommentCursors',
      path: 'CommentCursors',
      idName: 'id',
      properties: {
        "postId": {
          name: 'postId',
          type: 'string'
        },
        "type": {
          name: 'type',
          type: 'string'
        },
        "cursor": {
          name: 'cursor',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
