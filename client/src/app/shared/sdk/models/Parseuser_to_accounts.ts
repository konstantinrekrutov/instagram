/* tslint:disable */
import {
  ParseUserTasks,
  Account
} from '../index';

declare var Object: any;
export interface Parseuser_to_accountsInterface {
  "id"?: any;
  "taskId"?: any;
  "accountId"?: string;
  task?: ParseUserTasks;
  account?: Account;
}

export class Parseuser_to_accounts implements Parseuser_to_accountsInterface {
  "id": any;
  "taskId": any;
  "accountId": string;
  task: ParseUserTasks;
  account: Account;
  constructor(data?: Parseuser_to_accountsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Parseuser_to_accounts`.
   */
  public static getModelName() {
    return "Parseuser_to_accounts";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Parseuser_to_accounts for dynamic purposes.
  **/
  public static factory(data: Parseuser_to_accountsInterface): Parseuser_to_accounts{
    return new Parseuser_to_accounts(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Parseuser_to_accounts',
      plural: 'Parseuser_to_accounts',
      path: 'Parseuser_to_accounts',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "taskId": {
          name: 'taskId',
          type: 'any'
        },
        "accountId": {
          name: 'accountId',
          type: 'string'
        },
      },
      relations: {
        task: {
          name: 'task',
          type: 'ParseUserTasks',
          model: 'ParseUserTasks',
          relationType: 'belongsTo',
                  keyFrom: 'taskId',
          keyTo: 'id'
        },
        account: {
          name: 'account',
          type: 'Account',
          model: 'Account',
          relationType: 'belongsTo',
                  keyFrom: 'accountId',
          keyTo: 'id'
        },
      }
    }
  }
}
