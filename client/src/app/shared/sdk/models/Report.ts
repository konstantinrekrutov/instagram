/* tslint:disable */

declare var Object: any;
export interface ReportInterface {
  "filename": string;
  "date": Date;
  "from": Date;
  "id"?: any;
  "taskId"?: any;
  "taskType"?: string;
  "parseUserTasksId"?: any;
  task?: any;
}

export class Report implements ReportInterface {
  "filename": string;
  "date": Date;
  "from": Date;
  "id": any;
  "taskId": any;
  "taskType": string;
  "parseUserTasksId": any;
  task: any;
  constructor(data?: ReportInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Report`.
   */
  public static getModelName() {
    return "Report";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Report for dynamic purposes.
  **/
  public static factory(data: ReportInterface): Report{
    return new Report(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Report',
      plural: 'Reports',
      path: 'Reports',
      idName: 'id',
      properties: {
        "filename": {
          name: 'filename',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "from": {
          name: 'from',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "taskId": {
          name: 'taskId',
          type: 'any'
        },
        "taskType": {
          name: 'taskType',
          type: 'string'
        },
        "parseUserTasksId": {
          name: 'parseUserTasksId',
          type: 'any'
        },
      },
      relations: {
        task: {
          name: 'task',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'taskId',
          keyTo: 'id'
        },
      }
    }
  }
}
