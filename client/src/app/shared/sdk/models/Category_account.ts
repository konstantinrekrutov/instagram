/* tslint:disable */
import {
  Account,
  Category
} from '../index';

declare var Object: any;
export interface Category_accountInterface {
  "id"?: any;
  "accountId"?: string;
  "categoryId"?: any;
  account?: Account;
  category?: Category;
}

export class Category_account implements Category_accountInterface {
  "id": any;
  "accountId": string;
  "categoryId": any;
  account: Account;
  category: Category;
  constructor(data?: Category_accountInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Category_account`.
   */
  public static getModelName() {
    return "Category_account";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Category_account for dynamic purposes.
  **/
  public static factory(data: Category_accountInterface): Category_account{
    return new Category_account(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Category_account',
      plural: 'Category_accounts',
      path: 'Category_accounts',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "accountId": {
          name: 'accountId',
          type: 'string'
        },
        "categoryId": {
          name: 'categoryId',
          type: 'any'
        },
      },
      relations: {
        account: {
          name: 'account',
          type: 'Account',
          model: 'Account',
          relationType: 'belongsTo',
                  keyFrom: 'accountId',
          keyTo: 'id'
        },
        category: {
          name: 'category',
          type: 'Category',
          model: 'Category',
          relationType: 'belongsTo',
                  keyFrom: 'categoryId',
          keyTo: 'id'
        },
      }
    }
  }
}
