/* tslint:disable */
import {
  Comments,
  Pagination,
  Account
} from '../index';

declare var Object: any;
export interface PostsInterface {
  "postId": string;
  "code": string;
  "takenAt": number;
  "commentCount"?: number;
  "commentDisabled"?: boolean;
  "id"?: any;
  "accountId"?: string;
  "accountsId"?: string;
  comments?: Comments[];
  paginations?: Pagination;
  accounts?: Account;
}

export class Posts implements PostsInterface {
  "postId": string;
  "code": string;
  "takenAt": number;
  "commentCount": number;
  "commentDisabled": boolean;
  "id": any;
  "accountId": string;
  "accountsId": string;
  comments: Comments[];
  paginations: Pagination;
  accounts: Account;
  constructor(data?: PostsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Posts`.
   */
  public static getModelName() {
    return "Posts";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Posts for dynamic purposes.
  **/
  public static factory(data: PostsInterface): Posts{
    return new Posts(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Posts',
      plural: 'Posts',
      path: 'Posts',
      idName: 'id',
      properties: {
        "postId": {
          name: 'postId',
          type: 'string'
        },
        "code": {
          name: 'code',
          type: 'string'
        },
        "takenAt": {
          name: 'takenAt',
          type: 'number'
        },
        "commentCount": {
          name: 'commentCount',
          type: 'number'
        },
        "commentDisabled": {
          name: 'commentDisabled',
          type: 'boolean',
          default: false
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "accountId": {
          name: 'accountId',
          type: 'string'
        },
        "accountsId": {
          name: 'accountsId',
          type: 'string'
        },
      },
      relations: {
        comments: {
          name: 'comments',
          type: 'Comments[]',
          model: 'Comments',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'postsId'
        },
        paginations: {
          name: 'paginations',
          type: 'Pagination',
          model: 'Pagination',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'postsId'
        },
        accounts: {
          name: 'accounts',
          type: 'Account',
          model: 'Account',
          relationType: 'belongsTo',
                  keyFrom: 'accountsId',
          keyTo: 'id'
        },
      }
    }
  }
}
