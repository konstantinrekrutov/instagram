/* tslint:disable */
import {
  Task,
  ParseUserTasks
} from '../index';

declare var Object: any;
export interface InstagramInterface {
  "login": string;
  "logged"?: boolean;
  "password": string;
  "id"?: any;
  tasks?: Task[];
  parseUserTasks?: ParseUserTasks[];
}

export class Instagram implements InstagramInterface {
  "login": string;
  "logged": boolean;
  "password": string;
  "id": any;
  tasks: Task[];
  parseUserTasks: ParseUserTasks[];
  constructor(data?: InstagramInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Instagram`.
   */
  public static getModelName() {
    return "Instagram";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Instagram for dynamic purposes.
  **/
  public static factory(data: InstagramInterface): Instagram{
    return new Instagram(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Instagram',
      plural: 'Instagrams',
      path: 'Instagrams',
      idName: 'id',
      properties: {
        "login": {
          name: 'login',
          type: 'string'
        },
        "logged": {
          name: 'logged',
          type: 'boolean',
          default: false
        },
        "password": {
          name: 'password',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        tasks: {
          name: 'tasks',
          type: 'Task[]',
          model: 'Task',
          relationType: 'hasMany',
          modelThrough: 'Task_to_instagram',
          keyThrough: 'taskId',
          keyFrom: 'id',
          keyTo: 'instagramId'
        },
        parseUserTasks: {
          name: 'parseUserTasks',
          type: 'ParseUserTasks[]',
          model: 'ParseUserTasks',
          relationType: 'hasMany',
          modelThrough: 'Parseuser_to_instagram',
          keyThrough: 'taskId',
          keyFrom: 'id',
          keyTo: 'instagramId'
        },
      }
    }
  }
}
