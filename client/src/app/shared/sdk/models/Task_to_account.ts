/* tslint:disable */
import {
  Task,
  Account
} from '../index';

declare var Object: any;
export interface Task_to_accountInterface {
  "taskId": any;
  "accountId": string;
  "id"?: any;
  task?: Task;
  account?: Account;
}

export class Task_to_account implements Task_to_accountInterface {
  "taskId": any;
  "accountId": string;
  "id": any;
  task: Task;
  account: Account;
  constructor(data?: Task_to_accountInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Task_to_account`.
   */
  public static getModelName() {
    return "Task_to_account";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Task_to_account for dynamic purposes.
  **/
  public static factory(data: Task_to_accountInterface): Task_to_account{
    return new Task_to_account(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Task_to_account',
      plural: 'Task_to_accounts',
      path: 'Task_to_accounts',
      idName: 'id',
      properties: {
        "taskId": {
          name: 'taskId',
          type: 'any'
        },
        "accountId": {
          name: 'accountId',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        task: {
          name: 'task',
          type: 'Task',
          model: 'Task',
          relationType: 'belongsTo',
                  keyFrom: 'taskId',
          keyTo: 'id'
        },
        account: {
          name: 'account',
          type: 'Account',
          model: 'Account',
          relationType: 'belongsTo',
                  keyFrom: 'accountId',
          keyTo: 'id'
        },
      }
    }
  }
}
