/* tslint:disable */
import {
  Task,
  ParseUserTasks,
  MyUser,
  Account
} from '../index';

declare var Object: any;
export interface CategoryInterface {
  "name": string;
  "created": Date;
  "daysCountPost": number;
  "daysCountComments": number;
  "keywords"?: string;
  "minus"?: string;
  "id"?: any;
  "categoryId"?: any;
  "myUserId"?: any;
  category?: Category;
  tasks?: Task[];
  parseUserTasks?: ParseUserTasks[];
  myUser?: MyUser;
  accounts?: Account[];
}

export class Category implements CategoryInterface {
  "name": string;
  "created": Date;
  "daysCountPost": number;
  "daysCountComments": number;
  "keywords": string;
  "minus": string;
  "id": any;
  "categoryId": any;
  "myUserId": any;
  category: Category;
  tasks: Task[];
  parseUserTasks: ParseUserTasks[];
  myUser: MyUser;
  accounts: Account[];
  constructor(data?: CategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Category`.
   */
  public static getModelName() {
    return "Category";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Category for dynamic purposes.
  **/
  public static factory(data: CategoryInterface): Category{
    return new Category(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Category',
      plural: 'Categories',
      path: 'Categories',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "created": {
          name: 'created',
          type: 'Date',
          default: new Date(0)
        },
        "daysCountPost": {
          name: 'daysCountPost',
          type: 'number',
          default: 30
        },
        "daysCountComments": {
          name: 'daysCountComments',
          type: 'number',
          default: 7
        },
        "keywords": {
          name: 'keywords',
          type: 'string'
        },
        "minus": {
          name: 'minus',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "categoryId": {
          name: 'categoryId',
          type: 'any'
        },
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
      },
      relations: {
        category: {
          name: 'category',
          type: 'Category',
          model: 'Category',
          relationType: 'belongsTo',
                  keyFrom: 'categoryId',
          keyTo: 'id'
        },
        tasks: {
          name: 'tasks',
          type: 'Task[]',
          model: 'Task',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'categoryId'
        },
        parseUserTasks: {
          name: 'parseUserTasks',
          type: 'ParseUserTasks[]',
          model: 'ParseUserTasks',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'categoryId'
        },
        myUser: {
          name: 'myUser',
          type: 'MyUser',
          model: 'MyUser',
          relationType: 'belongsTo',
                  keyFrom: 'myUserId',
          keyTo: 'id'
        },
        accounts: {
          name: 'accounts',
          type: 'Account[]',
          model: 'Account',
          relationType: 'hasMany',
          modelThrough: 'Category_account',
          keyThrough: 'accountId',
          keyFrom: 'id',
          keyTo: 'categoryId'
        },
      }
    }
  }
}
