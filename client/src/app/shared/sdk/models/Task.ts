/* tslint:disable */
import {
  Instagram,
  Report,
  Account,
  Category
} from '../index';

declare var Object: any;
export interface TaskInterface {
  "daysCountPost": number;
  "keywords"?: string;
  "minus"?: string;
  "daysCountComments": number;
  "started"?: boolean;
  "name": string;
  "firstLapTime"?: Date;
  "id"?: any;
  "categoryId"?: any;
  instagrams?: Instagram[];
  reports?: Report[];
  accounts?: Account[];
  category?: Category;
}

export class Task implements TaskInterface {
  "daysCountPost": number;
  "keywords": string;
  "minus": string;
  "daysCountComments": number;
  "started": boolean;
  "name": string;
  "firstLapTime": Date;
  "id": any;
  "categoryId": any;
  instagrams: Instagram[];
  reports: Report[];
  accounts: Account[];
  category: Category;
  constructor(data?: TaskInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Task`.
   */
  public static getModelName() {
    return "Task";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Task for dynamic purposes.
  **/
  public static factory(data: TaskInterface): Task{
    return new Task(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Task',
      plural: 'Tasks',
      path: 'Tasks',
      idName: 'id',
      properties: {
        "daysCountPost": {
          name: 'daysCountPost',
          type: 'number',
          default: 30
        },
        "keywords": {
          name: 'keywords',
          type: 'string'
        },
        "minus": {
          name: 'minus',
          type: 'string'
        },
        "daysCountComments": {
          name: 'daysCountComments',
          type: 'number',
          default: 7
        },
        "started": {
          name: 'started',
          type: 'boolean',
          default: false
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "firstLapTime": {
          name: 'firstLapTime',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "categoryId": {
          name: 'categoryId',
          type: 'any'
        },
      },
      relations: {
        instagrams: {
          name: 'instagrams',
          type: 'Instagram[]',
          model: 'Instagram',
          relationType: 'hasMany',
          modelThrough: 'Task_to_instagram',
          keyThrough: 'instagramId',
          keyFrom: 'id',
          keyTo: 'taskId'
        },
        reports: {
          name: 'reports',
          type: 'Report[]',
          model: 'Report',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'taskId'
        },
        accounts: {
          name: 'accounts',
          type: 'Account[]',
          model: 'Account',
          relationType: 'hasMany',
          modelThrough: 'Task_to_account',
          keyThrough: 'accountId',
          keyFrom: 'id',
          keyTo: 'taskId'
        },
        category: {
          name: 'category',
          type: 'Category',
          model: 'Category',
          relationType: 'belongsTo',
                  keyFrom: 'categoryId',
          keyTo: 'id'
        },
      }
    }
  }
}
