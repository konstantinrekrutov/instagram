/* tslint:disable */
import {
  Instagram,
  Report,
  Account,
  Category
} from '../index';

declare var Object: any;
export interface ParseUserTasksInterface {
  "name": string;
  "started": boolean;
  "firstLapTime"?: Date;
  "id"?: any;
  "categoryId"?: any;
  instagrams?: Instagram[];
  reports?: Report[];
  accounts?: Account[];
  category?: Category;
}

export class ParseUserTasks implements ParseUserTasksInterface {
  "name": string;
  "started": boolean;
  "firstLapTime": Date;
  "id": any;
  "categoryId": any;
  instagrams: Instagram[];
  reports: Report[];
  accounts: Account[];
  category: Category;
  constructor(data?: ParseUserTasksInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ParseUserTasks`.
   */
  public static getModelName() {
    return "ParseUserTasks";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ParseUserTasks for dynamic purposes.
  **/
  public static factory(data: ParseUserTasksInterface): ParseUserTasks{
    return new ParseUserTasks(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ParseUserTasks',
      plural: 'ParseUserTasks',
      path: 'ParseUserTasks',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "started": {
          name: 'started',
          type: 'boolean',
          default: false
        },
        "firstLapTime": {
          name: 'firstLapTime',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "categoryId": {
          name: 'categoryId',
          type: 'any'
        },
      },
      relations: {
        instagrams: {
          name: 'instagrams',
          type: 'Instagram[]',
          model: 'Instagram',
          relationType: 'hasMany',
          modelThrough: 'Parseuser_to_instagram',
          keyThrough: 'instagramId',
          keyFrom: 'id',
          keyTo: 'taskId'
        },
        reports: {
          name: 'reports',
          type: 'Report[]',
          model: 'Report',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'parseUserTasksId'
        },
        accounts: {
          name: 'accounts',
          type: 'Account[]',
          model: 'Account',
          relationType: 'hasMany',
          modelThrough: 'Parseuser_to_accounts',
          keyThrough: 'accountId',
          keyFrom: 'id',
          keyTo: 'taskId'
        },
        category: {
          name: 'category',
          type: 'Category',
          model: 'Category',
          relationType: 'belongsTo',
                  keyFrom: 'categoryId',
          keyTo: 'id'
        },
      }
    }
  }
}
