/* tslint:disable */
import {
  Log
} from '../index';

declare var Object: any;
export interface StatInterface {
  "lastLap"?: Date;
  "info"?: any;
  "zaprosov"?: number;
  "limitZaprosov"?: number;
  "id"?: any;
  logs?: Log[];
}

export class Stat implements StatInterface {
  "lastLap": Date;
  "info": any;
  "zaprosov": number;
  "limitZaprosov": number;
  "id": any;
  logs: Log[];
  constructor(data?: StatInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Stat`.
   */
  public static getModelName() {
    return "Stat";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Stat for dynamic purposes.
  **/
  public static factory(data: StatInterface): Stat{
    return new Stat(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Stat',
      plural: 'Stats',
      path: 'Stats',
      idName: 'id',
      properties: {
        "lastLap": {
          name: 'lastLap',
          type: 'Date'
        },
        "info": {
          name: 'info',
          type: 'any',
          default: <any>null
        },
        "zaprosov": {
          name: 'zaprosov',
          type: 'number'
        },
        "limitZaprosov": {
          name: 'limitZaprosov',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        logs: {
          name: 'logs',
          type: 'Log[]',
          model: 'Log',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'statId'
        },
      }
    }
  }
}
