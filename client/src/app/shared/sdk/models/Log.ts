/* tslint:disable */
import {
  Stat
} from '../index';

declare var Object: any;
export interface LogInterface {
  "message": string;
  "status"?: number;
  "id"?: any;
  "statId"?: any;
  stat?: Stat;
}

export class Log implements LogInterface {
  "message": string;
  "status": number;
  "id": any;
  "statId": any;
  stat: Stat;
  constructor(data?: LogInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Log`.
   */
  public static getModelName() {
    return "Log";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Log for dynamic purposes.
  **/
  public static factory(data: LogInterface): Log{
    return new Log(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Log',
      plural: 'Logs',
      path: 'Logs',
      idName: 'id',
      properties: {
        "message": {
          name: 'message',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "statId": {
          name: 'statId',
          type: 'any'
        },
      },
      relations: {
        stat: {
          name: 'stat',
          type: 'Stat',
          model: 'Stat',
          relationType: 'belongsTo',
                  keyFrom: 'statId',
          keyTo: 'id'
        },
      }
    }
  }
}
