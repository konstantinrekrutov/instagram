/* tslint:disable */
import {
  Followers,
  ParseUserTasks,
  Account
} from '../index';

declare var Object: any;
export interface Followers_to_accountsInterface {
  "createdAt": Date;
  "required"?: any;
  "id"?: any;
  "followerId"?: number;
  "parseUserTasksId"?: any;
  "accountId"?: string;
  follower?: Followers;
  parseUserTasks?: ParseUserTasks;
  account?: Account;
}

export class Followers_to_accounts implements Followers_to_accountsInterface {
  "createdAt": Date;
  "required": any;
  "id": any;
  "followerId": number;
  "parseUserTasksId": any;
  "accountId": string;
  follower: Followers;
  parseUserTasks: ParseUserTasks;
  account: Account;
  constructor(data?: Followers_to_accountsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Followers_to_accounts`.
   */
  public static getModelName() {
    return "Followers_to_accounts";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Followers_to_accounts for dynamic purposes.
  **/
  public static factory(data: Followers_to_accountsInterface): Followers_to_accounts{
    return new Followers_to_accounts(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Followers_to_accounts',
      plural: 'Followers_to_accounts',
      path: 'Followers_to_accounts',
      idName: 'id',
      properties: {
        "createdAt": {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        "required": {
          name: 'required',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "followerId": {
          name: 'followerId',
          type: 'number'
        },
        "parseUserTasksId": {
          name: 'parseUserTasksId',
          type: 'any'
        },
        "accountId": {
          name: 'accountId',
          type: 'string'
        },
      },
      relations: {
        follower: {
          name: 'follower',
          type: 'Followers',
          model: 'Followers',
          relationType: 'belongsTo',
                  keyFrom: 'followerId',
          keyTo: 'id'
        },
        parseUserTasks: {
          name: 'parseUserTasks',
          type: 'ParseUserTasks',
          model: 'ParseUserTasks',
          relationType: 'belongsTo',
                  keyFrom: 'parseUserTasksId',
          keyTo: 'id'
        },
        account: {
          name: 'account',
          type: 'Account',
          model: 'Account',
          relationType: 'belongsTo',
                  keyFrom: 'accountId',
          keyTo: 'id'
        },
      }
    }
  }
}
