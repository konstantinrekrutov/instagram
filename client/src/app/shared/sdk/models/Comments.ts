/* tslint:disable */
import {
  Posts
} from '../index';

declare var Object: any;
export interface CommentsInterface {
  "commentId": number;
  "text": string;
  "username": string;
  "userId": string;
  "createdAt": number;
  "id"?: any;
  "postsId"?: any;
  posts?: Posts;
}

export class Comments implements CommentsInterface {
  "commentId": number;
  "text": string;
  "username": string;
  "userId": string;
  "createdAt": number;
  "id": any;
  "postsId": any;
  posts: Posts;
  constructor(data?: CommentsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Comments`.
   */
  public static getModelName() {
    return "Comments";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Comments for dynamic purposes.
  **/
  public static factory(data: CommentsInterface): Comments{
    return new Comments(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Comments',
      plural: 'Comments',
      path: 'Comments',
      idName: 'id',
      properties: {
        "commentId": {
          name: 'commentId',
          type: 'number'
        },
        "text": {
          name: 'text',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "userId": {
          name: 'userId',
          type: 'string'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "postsId": {
          name: 'postsId',
          type: 'any'
        },
      },
      relations: {
        posts: {
          name: 'posts',
          type: 'Posts',
          model: 'Posts',
          relationType: 'belongsTo',
                  keyFrom: 'postsId',
          keyTo: 'id'
        },
      }
    }
  }
}
