/* tslint:disable */

declare var Object: any;
export interface ControlsInterface {
  "stopped": boolean;
  "id"?: any;
}

export class Controls implements ControlsInterface {
  "stopped": boolean;
  "id": any;
  constructor(data?: ControlsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Controls`.
   */
  public static getModelName() {
    return "Controls";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Controls for dynamic purposes.
  **/
  public static factory(data: ControlsInterface): Controls{
    return new Controls(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Controls',
      plural: 'Controls',
      path: 'Controls',
      idName: 'id',
      properties: {
        "stopped": {
          name: 'stopped',
          type: 'boolean',
          default: false
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
