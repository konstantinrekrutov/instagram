module.exports = async function (server) {
    setTimeout(async () => {
        let controls = await server.models.Controls.findOne();
        if (!controls) {
            await server.models.Controls.create();
        }
        let statObj = await server.models.Stat.findOne();
        if (!statObj) {
            statObj = await server.models.Stat.create();
        }
        server.models.Looper.start(statObj);
    }, 5000);

};