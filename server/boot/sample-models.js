
module.exports = function(app) {
    const User = app.models.myUser;
    const Role = app.models.Role;
    const RoleMapping = app.models.RoleMapping;
    const Task = app.models.Task;

    // User.create( {username: 'Raf', email: 'timurvolnii@gmail.com', password: 'timurvolnii'}, {}, function(err, user) {
    //     if (err) throw err;
    //     console.log('Created users:', user);
    //     user.emailVerified = true;
    //     user.save();
    //     Role.create({
    //         name: 'admin'
    //     }, function(err, role) {
    //         if (err) throw err;
    //         role.principals.create({
    //             principalType: RoleMapping.USER,
    //             principalId: user.id
    //         }, function(err, principal) {
    //             if (err) throw err;
    //
    //             console.log('Created principal:', principal);
    //         });
    //     });
    // }, function (err) {
    //
    // });
};
Array.prototype.diff = function (a2) {
    let i;
    const a = [], diff = [];
    const a1 = this;
    for (i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (let k in a) {
        diff.push(k);
    }

    return diff;
};
Array.prototype.equals = function (array) {
    if (!array)
        return false;
    if (this.length !== array.length)
        return false;

    const sorted1 = this.sort();
    const sorted2 = array.sort();

    let i = 0, l = sorted1.length;
    for (; i < l; i++) {
        // Check if we have nested arrays
        if (sorted1[i] instanceof Array && sorted2[i] instanceof Array) {
            // recurse into the nested arrays
            if (!sorted1[i].equals(sorted2[i]))
                return false;
        }
        else if (sorted1[i] !== sorted2[i]) {
            return false;
        }
    }
    return true;
};
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});
Object.defineProperty(Array.prototype, "diff", {enumerable: false});
