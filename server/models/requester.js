'use strict';
const Client = require('instagram-private-api').V1;
const request = require('request');
const fs = require('fs');
const Request = Client.Request;
const instagramUrl = "https://www.instagram.com/";
// var HttpsProxyAgent = require('https-proxy-agent');
const urlencode = require('urlencode');
module.exports = function(Requester) {
    Requester.getQueryHash = function(type = 'comment') {
        return new Promise(function(resolve, reject) {
            try {
                const url = fs.readFileSync('./fileForQID', 'utf8');
                request.get(url, function(err, res, body) {
                    const matches = body.match(/queryId:[\s\t]*(.*?),/g);
                    const qids = matches.map(function(item) {
                        const qid = item.match(/:"(.*?)"/);
                        return qid[1];
                    })
                    switch (type) {
                        case 'comment' : {
                            return resolve(qids[0]);
                        }
                        default: {
                            return reject();
                        }
                    }
                })
            } catch (e) {
                reject(e);
            }

        })
    }

    Requester.getPosts = function(account_id, queryId, after, first) {

    }

};
