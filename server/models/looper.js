'use strict';
const Client = require('instagram-private-api').V1;
const ObjectID = require('mongodb').ObjectID;
module.exports = function (Looper) {
    let zaprosov = 0;
    let MAX_USERS_ON_PAGE = 196;
    let started = new Date();
    let statObj;
    Looper.start = async function (stat) {
        statObj = stat;
        await Looper.isStarted();
        Looper.loopPosts();
    }
    Looper.loopComments = function () {
        Looper.app.models.Task.prepareLoopData().then(function (accountsInTasks) {
            Looper.getCommentsFromPosts(accountsInTasks).then(function (res) {
                Looper.loopUsers().then(function () {
                    Looper.loopPosts();
                });
            }, function (err) {
                console.error(err);
            });
        });
    };
    Looper.interval = function (delay) {
        delay = delay ? delay : 200;
        return new Promise(resolve => {
            setTimeout(() => {
                resolve();
            }, delay);
        });
    };

    Looper.clearStats = async function () {
        await statObj.logs.destroyAll();
        statObj.lastLap = new Date();
        statObj.zaprosov = 0;
        statObj.limitZaprosov = 0;
        statObj = await statObj.save();
    };
    Looper.loopUsers = async function () {
        await Looper.isStarted();

        const Instagram = Looper.app.models.Instagram;
        const tasks = await Looper.app.models.ParseUserTasks.find({
            started: true,
            include: ['accounts', 'instagrams'],
        });
        if (!tasks) {
            return;
        }
        for (const i in tasks) {

            const task = tasks[i];
            await Looper.setStat('starting task ' + task['name']);
            const accounts = await task['accounts'].find();
            const instagrams = await task['instagrams'].find();
            const troughModel = Looper.app.models.followers_to_accounts;
            let j;
            for (j= 0; j < accounts.length; j++) {
                const rand = Instagram.getRandomInt(0, instagrams.length);
                const account = accounts[j];
                await Looper.setStat(j +' get followers for account ' + account['username']);
                const followersCount = await account.followers.count();
                await Looper.setStat(account['username'] + ' have ' + followersCount + ' followers');

                const session = await Instagram.getSession(instagrams[rand]);
                const followersFeed = new Client.Feed.AccountFollowers(session, account.id);
                let followers;
                try {
                    await Looper.interval(1000);
                    if (followersCount > 0) {
                        console.log('get last followers of ' + account['username']);
                        await Looper.addZapros();
                        followers = await followersFeed.get();
                    } else {
                        console.log('get 2 followers of ' + account['username']);
                        followers = await followersFeed.get();
                        await Looper.addZapros();
                        let followerspage2;
                        for(let l = 0; l < 5; l++) {
                            if(!followersFeed.moreAvailable) {
                                break;
                            }
                            await Looper.interval(1000);
                            followerspage2 = await followersFeed.get();
                            await Looper.addZapros();
                            if(!followerspage2.length) {
                                await Looper.setStat('getted ' + followers.length + ' followers');
                                break;
                            }
                            followerspage2.forEach((item) => {
                                followers.push(item);
                            });
                        }
                    }

                    const followersModel = followers.map((follower) => {
                        return {
                            _id: follower.pk,
                            username: follower.username,
                        };
                    });
                    let exists = await Looper.app.models.Followers.find({
                        where: {
                            id: {
                                inq: followersModel.map(item => {
                                        return item._id;
                                    },
                                ),
                            },
                        },
                    });
                    let newModels;
                    if (exists.length) {
                        const existsIds = exists.map((item) => {
                            return item.id;
                        });
                        const existsUserIds = exists.map((item) => {
                            return item.id;
                        });
                        newModels = followersModel.filter((item) => {
                            return existsUserIds.indexOf(item._id) === -1;
                        });
                        const existsLinks = await troughModel.find({
                            where: {
                                followerId: {inq: existsIds},
                                parseUserTasksId:task.id,
                                accountId: account.id
                            },
                        });
                        let nonExistLinks;
                        if (existsLinks.length) {
                            const existsLinksIds = existsLinks.map(item => {
                                return item.followerId.toString();
                            });
                            nonExistLinks = existsIds.filter(item => {
                                return existsLinksIds.indexOf(item.toString()) === -1;
                            });
                        } else {
                            nonExistLinks = existsIds;
                        }
                        if (nonExistLinks.length) {
                            const links = await troughModel.create(nonExistLinks.map(item => {
                                return {
                                    accountId: account.id,
                                    followerId: item,
                                    parseUserTasksId:task.id,
                                    createdAt: new Date()
                                };
                            }));
                            await Looper.setStat('linked new followers ' + links.length);
                        }
                    } else {
                        newModels = followersModel;
                    }
                    if (newModels.length) {
                        await Looper.insertBatchModels(newModels, task, account);

                    }
                } catch (e) {
                    Looper.limitZapros(e, {task: Looper.loopUsers});
                    await Looper.isStarted(120);
                    if(e && e.name == 'ParseError') {
                        j--;
                    }
                }
                await Looper.isStarted();

            }
            await Looper.setStat('end task ' + task['name']);
            await Looper.isStarted();
            if (!task.firstLapTime) {
                task.firstLapTime = new Date();
                await task.save();
            }
        }
        await Looper.clearStats();

    };
    Looper.insertBatchModels = function(newModels, task, account) {
        return new Promise(function (resolve, reject) {
            Looper.app.dataSources.mongo.connector.connect(function (err, db) {
                let collection = db.collection('followers');
                collection.insertMany(newModels, function(err, res) {
                    if(err) {
                        resolve();
                        return console.error(err);
                    }
                    const links = [];
                    collection = db.collection('followers_to_accounts');
                    for(const i in res.insertedIds) {
                        const item = res.insertedIds[i];
                        links.push({
                            accountId: account.id,
                            parseUserTasksId: task.id,
                            followerId: item,
                            createdAt: new Date()
                        })
                    }
                    collection.insertMany(links, function(err, res) {
                        if(err) {
                            resolve();
                            return console.error(err);
                        }
                        Looper.setStat('added new followers ' + res.insertedCount);
                        return resolve();
                    });
                });

            })
        })
    }
    Looper.getOldCommentsFromPost = function (post, instagram, params, zapros) {
        return new Promise(async function (resolve) {
            zapros = zapros ? zapros : 0;
            if(zapros && zapros > 2) {
                resolve();
            }
            zapros++;
            const Comments = Looper.app.models.Comments;
            try {
                const res = await Comments.getOldComments(post.postId, instagram, params);

                const commentsModel = [];
                await Looper.setStat('old comments from post ' + post['code'] + ' getted');
                for (const k in res) {
                    const comment = res[k];
                    commentsModel.push({
                        commentId: comment['pk'],
                        createdAt: comment['created_at'],
                        username: comment['user']['username'],
                        userId: comment['user']['pk'],
                        text: comment['text'],
                    });
                }
                if (!commentsModel.length) {
                    return resolve();
                }
                post.comments.create(commentsModel, function (err, rs) {
                    if (err) {
                        console.error(err);
                    }
                });
                resolve();
            } catch (e) {
                if (e && e.name =='MediaUnavailableError') {
                    Looper.app.models.posts.deleteById(post.id, function (err, res) {
                        resolve();
                    });
                }  else if(e && e.name === 'ParseError') {
                    Looper.limitZapros(e, {task: Looper.loopComments});
                    await Looper.isStarted(120);
                    Looper.getCommentsFromPost(post, instagram, params, zapros).then(function () {
                        resolve();
                    });
                } else {
                    resolve();
                }
            }
        });
    }
    Looper.getCommentsFromPost = function (post, instagram, params, zapros) {
        return new Promise(async function (resolve) {
            zapros = zapros ? zapros : 0;
            if(zapros && zapros > 2) {
                resolve();
            }
            zapros++;
            const Comments = Looper.app.models.Comments;
            try {
                const res = await Comments.getComments(post.postId, instagram, params);

                const commentsModel = [];
                await Looper.setStat('comments from post ' + post['code'] + ' getted');
                for (const k in res) {
                    const comment = res[k];
                    commentsModel.push({
                        commentId: comment['pk'],
                        createdAt: comment['created_at'],
                        username: comment['user']['username'],
                        userId: comment['user']['pk'],
                        text: comment['text'],
                    });
                }
                if (!commentsModel.length) {
                    return resolve();
                }
                post.comments.create(commentsModel, function (err, rs) {
                    if (err) {
                        console.error(err);
                    }
                });
                resolve();
            } catch (e) {
                if (e && e.name =='MediaUnavailableError') {
                    Looper.app.models.posts.deleteById(post.id, function (err, res) {
                        resolve();
                    });
                }  else if(e && e.name === 'ParseError') {
                    Looper.limitZapros(e, {task: Looper.loopComments});
                    await Looper.isStarted(120);
                    Looper.getCommentsFromPost(post, instagram, params, zapros).then(function () {
                        resolve();
                    });
                } else {
                    resolve();
                }
            }
        })

    };
    Looper.getCommentsFromPosts = async function (accountsInTasks) {
        await Looper.isStarted();
        const Instagram = Looper.app.models.Instagram;
        for (const i in accountsInTasks) {
            const task = accountsInTasks[i];
            await Looper.setStat('get comments from task ' + task['task']['name']);
            const account = accountsInTasks[i]['account'];
            const rand = task.instagrams.length > 1 ? Instagram.getRandomInt(0, task.instagrams.length) : 0;

            const takenAtPosts = new Date();
            takenAtPosts.setDate(takenAtPosts.getDate() - task.daysCountPost);

            const takenAtComments = new Date();
            takenAtComments.setDate(takenAtComments.getDate() - task.daysCountComments);
            const posts = await account.posts.find({where: {takenAt: {gt: Math.floor(takenAtPosts.getTime() / 1000)}}});
            if (!posts) {
                await Looper.setStat('no posts  ' + task['task']['name']);
                continue;
            }
            for (const j in posts) {
                await Looper.isStarted();
                const post = posts[j];
                await Looper.setStat('get comments from post ' + post['code']);
                const firstComment = await post.comments.findOne({order: 'createdAt ASC'});
                const lastComment = await post.comments.findOne({order: 'createdAt DESC'});
                if(firstComment) {
                    const dateFromComments = firstComment['createdAt'];
                    const from = takenAtComments.getTime() / 1000;
                    const zazor = dateFromComments - from;
                    if(zazor > 86400) {
                        await Looper.getOldCommentsFromPost(post, task.instagrams[rand], {from:from, to: dateFromComments, firstComment: firstComment});
                    }
                }

                const params = {
                    takenAt: Math.floor(takenAtComments.getTime() / 1000),
                };
                if (lastComment) {
                    params['lastComment'] = lastComment;
                    params['takenAt'] = lastComment['createdAt'];
                }
                await Looper.getCommentsFromPost(post, task.instagrams[rand], params);
            }
            if (!task['task'].firstLapTime) {
                task['task'].firstLapTime = new Date();
                await task['task'].save();
            }
            await Looper.isStarted();

        }
        await Looper.setStat('get comments finished');
    };
    Looper.loopPosts = function () {
        Looper.app.models.Task.prepareLoopData().then(function (accountsInTasks) {
            Looper.startTasks(accountsInTasks).then(function (res) {
                Looper.loopComments();
            }, function (err) {
                throw new Error(err);
            });
        });
    };

    Looper.addPosts = function (task, account, posts, lastPost) {
        const postModels = [];
        for (const postId in posts) {
            const post = posts[postId];
            if (lastPost && lastPost.takenAt >= post.taken_at) {
                return;
            }
            const activeTo = new Date(post.taken_at);
            activeTo.setDate(activeTo.getDate() + task.daysCountPost);

            postModels.push({
                postId: post.id,
                code: post.code,
                takenAt: post.taken_at,
                activeTo: activeTo,
                commentCount: post.comment_count,
                commentDisabled: post.comments_disabled,
            });
        }
        if (postModels.length) {
            account.posts.create(postModels, async (err, post) => {
                if (err) {
                    console.error(err);
                } else {
                    await Looper.setStat('added new posts');
                }
            });

        }
    };

    Looper.startTasks = async function (accountsInTasks) {
        await Looper.isStarted();
        const Instagram = Looper.app.models.Instagram;
        await Looper.setStat('starting get posts');
        for (const i in accountsInTasks) {
            try {
                await Looper.interval();
                const task = accountsInTasks[i];
                const account = accountsInTasks[i]['account'];
                await Looper.setStat('get posts for ' + account.username);
                const rand = task.instagrams.length > 1 ? Instagram.getRandomInt(0, task.instagrams.length) : 0;
                let takenAt;
                const from = await account.historyFroms.get();
                takenAt = new Date();
                takenAt.setDate(takenAt.getDate() - task.daysCountPost);
                takenAt = takenAt.getTime();
                if (!from) {
                    await account.historyFroms.create({time: takenAt});
                } else if (from.time > takenAt) {
                    await Looper.setStat('get older posts');
                    const firstPost = await account.posts.findOne({order: 'takenAt ASC'});
                    let takenAtTo = firstPost ? firstPost.takenAt : new Date().getTime();
                    await Looper.interval();
                    const posts = await Looper.app.models.Instagram.getPosts(account.id, task.instagrams[rand], {
                        firstPost: firstPost,
                        takenAtTo: takenAtTo,
                        takenAtFrom: takenAt,
                    });

                    const historyFrom = await account.historyFroms.get();
                    historyFrom.time = takenAt;
                    historyFrom.save(function (err) {
                        console.log(err);
                    });
                    Looper.addPosts(task, account, posts, null);
                }
                const lastPost = await account.posts.findOne({order: 'takenAt DESC'});
                if (lastPost) {
                    takenAt = lastPost.takenAt;
                } else {
                    takenAt = new Date();
                    takenAt.setDate(takenAt.getDate() - task.daysCountPost);
                    takenAt = Math.floor(takenAt.getTime() / 1000);
                }
                const posts = await Looper.getPosts(account.id, task.instagrams[rand], {
                    lastPost: lastPost,
                    takenAt: takenAt,
                });
                await Looper.setStat('posts for ' + account.username + ' getted');
                Looper.addPosts(task, account, posts, lastPost);
            } catch (e) {
                Looper.limitZapros(e, {task: Looper.loopPosts});
                await Looper.isStarted(120);
            }
            await Looper.isStarted();
        }
        await Looper.setStat('get posts finished');

    };

    Looper.getPosts = function (accountId, instagram, params, zapros) {
        zapros = zapros ? zapros : 0;
        return new Promise(async function (resolve, reject) {
            if(zapros > 2) {
                return resolve([]);
            }
            try {
                const posts = await Looper.app.models.Instagram.getPosts(accountId, instagram, params);
                resolve(posts);
            } catch (e) {
                Looper.limitZapros(e, {task: Looper.loopPosts});
                await Looper.isStarted(120);
                if(e && e.name === 'ParseError') {
                    Looper.getPosts(accountId, instagram, params).then(function (posts) {
                        resolve(posts);
                    })
                } else {
                    resolve([]);
                }

            }
        });
    }
    Looper.setStat = async function (info) {
        console.log(info);
        await statObj.logs.create({message: info, status: 0})
    };

    Looper.setLimitZaprosov = async function (info) {
        statObj.limitZaprosov = info;
        await statObj.save();
    };
    Looper.setStatZaprosov = async function (info) {
        statObj.zaprosov = info;
        await statObj.save();
    };
    Looper.pause = async function (delay) {
        return new Promise(async function (resolve, reject) {
            setTimeout(() => {
                resolve();
            }, delay * 1000);
        });
    };
    Looper.isStarted = async function (delay) {
        return new Promise(async function (resolve, reject) {
            if (delay) {
                await Looper.pause(delay);
            }
            let controls = await Looper.app.models.Controls.findOne();
            if (controls.stopped) {
                let interval = setInterval(async () => {
                    controls = await Looper.app.models.Controls.findOne();
                    if (!controls.stopped) {
                        clearInterval(interval);
                        resolve();
                    }
                }, 1000 * 20);
            } else {
                resolve();
            }
        })
    };

    Looper.addZapros = async function (cnt) {
        if (cnt) {
            zaprosov += cnt;
        } else {
            zaprosov++;
        }
        await Looper.setStatZaprosov(zaprosov);
        console.log(zaprosov);
    };

    Looper.limitZapros = function (e, options) {
        Looper.setLimitZaprosov(zaprosov);
        console.error(e, started, options, zaprosov);
    };
};
