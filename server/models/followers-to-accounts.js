'use strict';

module.exports = function(Followerstoaccounts) {
  Followerstoaccounts.observe('before save', function filterProperties(ctx, next) {
    if (ctx.instance) {
      ctx.instance.createdAt = new Date();
    }
    next();
  });
};
