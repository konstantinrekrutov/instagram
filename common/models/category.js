'use strict';

module.exports = function(Category) {
    function editCategory(body){
        return new Promise(async function(resolve) {
            let logins = body.accounts.split('\n').map(item => {
                return item.trim();
            });
            const category = await Category.findById(body.id, {include: 'accounts'});
            delete body.accounts;
            delete body.id;
            const oldAccounts = await category.accounts();
            const oldAccountsU = oldAccounts.map(item => {
                return item.username;
            });
            category.updateAttributes(body, function(err, res) {
                console.log(err, res);
            });
            if (logins.equals(oldAccountsU)) {
                return resolve(null, {});
            } else {
                const map = {};
                oldAccounts.forEach(item => {
                    map[item['username']] = item;
                });
                const addedLogins = logins.filter(item => {
                    return oldAccountsU.indexOf(item) === -1;
                });
                const deletedLogins = oldAccountsU.filter(item => {
                    return logins.indexOf(item) === -1;
                });
                const troughtModel = Category.app.models.category_account;
                if (deletedLogins.length) {
                    const deletedIds = deletedLogins.map(item => {
                        return map[item].id;
                    });
                    await troughtModel.destroyAll({
                        categoryId: category.id,
                        accountId: {inq: deletedIds},
                    });
                }
                if (addedLogins.length) {
                    const addedAccounts = await Category.app.models.Account.getAccounts(addedLogins);
                    if (addedAccounts.length) {
                        await troughtModel.create(addedAccounts.map(item => {
                            return {
                                categoryId: category.id,
                                accountId: item.id
                            };
                        }));
                    }
                }
            }
            return resolve();
        })
    }
    Category.editCategory = function(body, cb) {
        editCategory(body).then(function() {
            cb();
        })
    };
    Category.setUser = function(categoryId, userId, cb) {
        Category.findById(categoryId, function(err, category) {
            if (err || !category) {
                return cb({code: '404'});
            }
            category.myUserId = userId;
            category.save(function(err) {
                if (err) {
                    return cb({code: '500'});
                }
                cb(null, category);
            });
        });
    };
    Category.addNew = function (category, cb) {
        let logins = [];
        category.accounts.split("\n").forEach(item => {
            item = item.trim();
            if(logins.indexOf(item) === -1) {
                logins.push(item);
            }
        });
        delete category.accounts;
        Category.create(category, function (err, category) {
            const Account = Category.app.models.Account;
            Account.addNew(logins, function (error, accounts) {
                const links = accounts.map(item => {
                    return {accountId: item.id, categoryId: category.id}
                });
                Category.app.dataSources.mongo.connector.connect(function (err, db) {
                    let collection = db.collection('category_account');
                    collection.insertMany(links, function (err, res) {
                        cb(err, category);
                    })
                });
            });
        })


    };
    Category.remoteMethod('setUser', {
        accepts: [{type:'string', arg:'categoryId'},{type:'string', arg:'userId'}],
        returns: {type:'object', root: true}
    });
    Category.remoteMethod('editCategory', {
        accepts: {type:'object', http: { source: 'body' } , arg:'category'},
        returns: {type:'object', root: true},
        http: {verb: 'post'}
    });
    Category.remoteMethod('addNew', {
        accepts: {type:'object', http:{source:'body'}, arg:'category'},
        returns: {type:'object', root: true}
    });
};
