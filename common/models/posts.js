'use strict';

module.exports = function(Posts) {
    Posts.validatesUniquenessOf('postId', {message: 'postId is not unique'});
};
