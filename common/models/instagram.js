'use strict';
const Client = require('instagram-private-api').V1;
const sessionsCache = {};
const request = require('request');

const instaUrl = 'https://www.instagram.com/';
module.exports = function (Instagram) {
    Instagram.validatesUniquenessOf('login', {message: 'login is not unique'});
    Instagram.removeAll = function(filter, cb) {
        Instagram.destroyAll(filter,cb);
    }
    Instagram.remoteMethod('removeAll', {
        accepts: {arg:'filter', type: 'object'},
        returns: {type: 'object', root: true},
        http: {verb: "del"},
    });
    Instagram.getSession = function (instagram) {
        return new Promise(function (resolve, reject) {
            if (sessionsCache[instagram.login]) {
                return resolve(sessionsCache[instagram.login]);
            }
            const device = new Client.Device(instagram.login);
            const storage = new Client.CookieFileStorage(__dirname + '../../../cookies/' + instagram.login + '.json');
            const session = new Client.Session(device, storage);
            session.getAccount()
                .then(account => {
                    sessionsCache[instagram.login] = session;
                    resolve(session);
                }, err => {
                    console.log(err);
                    Client.Session.create(device, storage, instagram.login, instagram.password)
                        .then(function (session) {
                            sessionsCache[instagram.login] = session;
                            resolve(session);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });

        });
    };
    Instagram.authorize = function (id, cb) {
        Instagram.findById(id, function (err, instagram) {
            Instagram.getSession(instagram).then(function (session) {
                instagram.logged = true;
                instagram.save();
                cb(null, {code: 'logged_id'});
            }, function (error) {
                cb({ error:true, code:error.name});
            }).catch(err => {
                cb({ error:true, code:error.name});

            });
        });

    };
    Instagram.addAccount = function (login, password, cb) {
        Instagram.create({login: login, password: password}, function (err, res) {
            if (err) {
                return cb({code: 'duplicate_error', error: true});
            }
            cb(null, res);
        });
    };
    Instagram.remoteMethod('addAccount', {
        accepts: [{arg: 'login', type: 'string'}, {
            arg: 'password',
            type: 'string',
        }],
        returns: {type: 'object', root: true},
    });
    Instagram.remoteMethod('authorize', {
        accepts: {arg: 'account', type: 'string'},
        returns: {type: 'object', root: true},
    });

    Instagram.getToDate = function (feed, takenAtFrom, takenAtTo,allResults, cb) {
        Instagram.app.models.Looper.addZapros();
        feed.get()
            .then(async function (results) {
                if (!results || !results.length) {
                    return cb(null, allResults);
                }
                for (const i in results) {
                    const item = results[i];
                    if (item.taken_at > takenAtFrom && item.taken_at < takenAtTo) {
                        allResults[item.pk] = item;
                    } else {
                        return cb(null, allResults);
                    }
                }
                if(feed.moreAvailable) {
                    await Instagram.app.models.Looper.interval();
                    Instagram.getToDate(feed, takenAtFrom, takenAtTo, allResults, cb);
                } else {
                    return cb(null, allResults);

                }
            }, function (err) {
                return cb(err, allResults);
            });
    }
    Instagram.getPosts = function (accountId, instagram, options = {}) {
        return new Promise(function (resolve, reject) {
            Instagram.getSession(instagram).then(function (session) {
                const feed = new Client.Feed.UserMedia(session, accountId);
                if (options.lastPost) {
                    feed.setCursor(options.lastPost.postId);
                }
                const allResults = {};
                if (options.takenAtTo && options.takenAtFrom) {
                    if(options.firstPost) {
                        feed.setCursor(options.firstPost.postId);
                    }
                    Instagram.getToDate(feed, options.takenAtFrom, options.takenAtTo, allResults, function (err) {
                        if (err) {
                            return reject(allResults);
                        }
                        resolve(allResults);
                    }, function (err) {
                        console.error(err)
                    });
                } else {
                    if (!options.takenAt && !options.firstPost) {
                        options.takenAt = new Date().getTime()
                    }
                    Instagram.getFromDate(feed, options.takenAt, allResults, function (err) {
                        if (err) {
                            return reject(err);
                        }
                        resolve(allResults);
                    }, function (err) {
                        console.error(err)
                    });
                }


            }, function (err) {
                reject(err);
            });
        });
    };
    Instagram.getFromDate = function (feed, takenAt, allResults, cb) {
        Instagram.app.models.Looper.addZapros();
        feed.get()
            .then( async function (results) {
                if (!results || !results.length) {
                    return cb(null, allResults);
                }
                for (const i in results) {
                    const item = results[i];
                    if (item.taken_at > takenAt) {
                        if(allResults[item.pk]) {
                            return cb('fff');

                        }
                        allResults[item.pk] = item;
                    } else {
                        return cb(null, allResults);
                    }
                }
                if(feed.moreAvailable) {
                    await Instagram.app.models.Looper.interval();
                    Instagram.getFromDate(feed, takenAt, allResults, cb);
                } else {
                    return cb(null, allResults);
                }
            }, function (err) {
                return cb(err, allResults);
            });
    };
    Instagram.getAccount = function(accountId, instagram) {
        return new Promise(function(resolve, reject) {
            Instagram.getSession(instagram).then(function(session) {
                Client.Account.getById(session, accountId).then(function(res) {
                    resolve(res);
                });
            });
        });

    };
    Instagram.getUserIds = function (logins) {
        const promises = [];
        let loginsUnique = []
        loginsUnique = logins.filter(item => {
            return loginsUnique.indexOf(item) === -1;
        });
        return new Promise(function (resolve, reject) {
            const result = {};
            loginsUnique.forEach(function (login) {
                promises.push(new Promise(function (resolve, reject) {
                    request.get(instaUrl + login, function (res, x, c) {
                        try {
                            const matches = c.match(/window._sharedData[\s\t]*=[s\t]*(.*?);<\/script/);
                            if (matches && matches[1]) {
                                try {
                                    const sharedInfo = JSON.parse(matches[1].trim());
                                    result[login] = sharedInfo.entry_data.ProfilePage[0]['graphql']['user']['id'];
                                    resolve({
                                        id: sharedInfo.entry_data.ProfilePage[0]['graphql']['user']['id'],
                                        username: login,
                                    });
                                } catch (e) {
                                    result[login] = null;
                                    resolve();
                                }
                            } else {
                                result[login] = null;
                                resolve();
                            }
                        } catch (e) {
                            console.error(instaUrl + login, e);
                        }

                    });
                }));

            });
            Promise.all(promises).then(function (responses) {
                responses = responses.filter(item => {
                    return item;
                });
                resolve(responses);
            }, function (errors) {
                reject(errors);
            });
        });
    };
    Instagram.getRandomInt = function(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
};
