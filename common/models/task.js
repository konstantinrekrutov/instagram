'use strict';

module.exports = function (Task) {
    Task.start = function (taskId, cb) {
        Task.findById(taskId, function (err, task) {
            if (err || !task) {
                return cb({code: 'task_not_found', error: true});
            }
            task.started = true;
            task.save(function (err, res) {
                if (err) {
                    console.log(err);
                    return cb({code: 'server_error', error: true});
                }
                return cb(null, {code: 'task_started'});

            });
        });
    };


    Task.editTask = function (body, cb) {
        if (!body.task.id) {
            return cb({code: 'id_is_required'})
        }
        const troughtModel = Task.app.models.task_to_account;
        const newTask = body.task;

        let nextAccountsU = [];
        body.accounts.split("\n").forEach(item => {
            item = item.trim();
            if(nextAccountsU.indexOf(item) === -1) {
                nextAccountsU.push(item);
            }
        });

        const instagrams = body.instagrams;
        const prevAccounts = newTask.accounts;
        const prevAccountsU = newTask.accounts.map(function (acc) {
            return acc.username;
        });

        delete newTask.accounts;
        Task.findById(newTask.id, function (err, task) {
            if (err || !task) {
                return cb({code: 'server_error', error: true});
            }
            const promises = [];
            if(!instagrams.equals(newTask.instagrams)) {
                promises.push(new Promise(function (resolve, reject) {
                    task.instagrams.destroyAll(function (err) {
                        if (err) {
                            return reject(err);
                        }
                        const troughModel = Task.app.models.task_to_instagram;
                        troughModel.create(instagrams.map(function (item) {
                            return {taskId: task.id, instagramId: item};
                        }), function (err) {
                            if (err) {
                                return reject(err);
                            }
                            resolve();

                        })
                    });
                }))
            }

            if (!nextAccountsU.equals(prevAccountsU)) {
                const newAccountsU = [];
                nextAccountsU.forEach(item => {
                    if(prevAccountsU.indexOf(item) === -1) {
                        newAccountsU.push(item);
                    }
                });
                if(newAccountsU.length) {
                    promises.push(new Promise(function (resolve) {
                        Task.app.models.Account.find({where: {username: {inq:newAccountsU}}}, function (err, isset) {
                            let nonExist = [];
                            const issetU = isset.map(item => {
                                return item.username;
                            });

                            if (isset && isset.length) {
                                troughtModel.create(isset.map(item => {
                                    return {accountId: item.id, taskId: task.id};
                                }), function(err, res) {
                                    if(err) {
                                        console.error(err);
                                    }
                                });
                                newAccountsU.forEach(item => {
                                    if (issetU.indexOf(item) === -1) {
                                        nonExist.push(item);
                                    }
                                });
                            } else {
                                nonExist = newAccountsU;
                            }
                            if(!nonExist.length) {
                                return resolve();
                            }
                            Task.app.models.Instagram.getUserIds(nonExist).then(function (accounts) {
                                if(!accounts) {
                                    return resolve([]);
                                }
                                accounts = accounts.filter(item => {
                                    return item && item.username;
                                });
                                if(!accounts.length) {
                                    return resolve([]);
                                }
                                accounts.forEach(function (item) {
                                    promises.push(new Promise(function (resolve, reject) {
                                        task.accounts.create(item, function (err, res) {
                                            if (err) {
                                                return reject(err);
                                            }
                                            resolve(res);
                                        });
                                    }));
                                });
                            }, function (err) {
                                console.error(err);
                                resolve();
                            })
                        });
                    }));
                }

                const deletedAccountsIds = [];
                prevAccounts.forEach(item => {
                    if(nextAccountsU.indexOf(item.username) === -1) {
                        deletedAccountsIds.push(item.id);
                    }
                });
                if(deletedAccountsIds.length) {
                    promises.push(new Promise(function (resolve, reject) {
                        troughtModel.destroyAll({taskId:task.id, accountId:{inq:deletedAccountsIds}}, function(err, res) {
                            if(err) {
                                console.error(err);
                            }
                            resolve();
                        })
                    }));
                }

            }
            promises.push(new Promise(function (resolve, reject) {
                task.updateAttributes(newTask, function (err, res) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(res);
                })
            }));
            Promise.all(promises).then(function (res) {
                cb(null, res);
            }, function (err) {
                cb(err);
            })
        });

    };
    Task.prepareLoopData = function () {
        return new Promise(function (resolve, reject) {
            Task.find({
                where: {started: true},
                include: ['instagrams', 'accounts']
            }, function (err, tasks) {
                if (err) {
                    return reject(err);
                }
                if (!tasks) {
                    return reject('tasks not found');
                }
                const accountsInTasks = {};
                const promises = [];
                tasks.forEach(function (task) {
                    promises.push(new Promise(function (resolve) {
                        task.accounts.find(function (err, accounts) {
                            task.instagrams.find(function (err, instagrams) {
                                accounts.forEach(function (account) {
                                    if (accountsInTasks[account.id]) {
                                        if (task.daysCountPost > accountsInTasks[account.id].daysCountPost) {
                                            accountsInTasks[account.id].daysCountPost = task.daysCountPost;
                                        }
                                        if (task.daysCountComments > accountsInTasks[account.id].daysCountComments) {
                                            accountsInTasks[account.id].daysCountComments = task.daysCountComments;
                                        }
                                        joinInstagrams(instagrams, accountsInTasks[account.id].instagrams);
                                    } else {
                                        accountsInTasks[account.id] = {
                                            account: account,
                                            task: task,
                                            instagrams: instagrams,
                                            daysCountPost: task.daysCountPost,
                                            daysCountComments: task.daysCountComments,
                                            keywords: task.keywords
                                        };
                                    }
                                });
                                resolve();
                            });
                        });

                    }))
                });
                Promise.all(promises).then(function () {
                    resolve(accountsInTasks);
                })
            });
        })
    };
    Task.addNew = function(task, cb) {
        const nextAccountsNU = task.accounts.split("\n").map(item => {return item.trim()});
        const accounts = nextAccountsNU.filter((item, key, self) => {
            return self.indexOf(item) === -1
        });
        const instagrams = task.instagrams;
        delete task.accounts;
        delete task.instagrams;
        Task.create(task, async function(err, task) {
            if (err) {
                console.log(err);
                return cb({code: 'server_error', error: true});
            }
            const instagramTroughModel = Task.app.models.task_to_instagram;
            instagramTroughModel.create(instagrams.map(function(item) {
                return {taskId: task.id, instagramId: item};
            }), function(err) {
                if (err) {
                    console.error(err);
                }
            });
            const accountsModels = await Task.app.models.Account.getAccounts(accounts);

            const accountTroughModel = Task.app.models.task_to_account;
            accountTroughModel.create(accountsModels.map(function(item) {
                return {taskId: task.id, accountId: item.id};
            }), function(err) {
                if (err) {
                    console.log(err);
                    return cb({code: 'server_error', error: true});
                }
                return cb(err, task);

            });
        });

    };
    Task.removeAll = function(filter, cb) {
        Task.destroyAll(filter, cb);
    };
    Task.remoteMethod('removeAll', {
        accepts: {arg:'filter', type: 'object'},
        returns: {type: 'object', root: true},
        http: {verb: "del"},
    });

    Task.remoteMethod('editTask', {
        accepts: {arg: 'body', type: 'object', http: {source: 'body'}},
        returns: {type: 'object', root: true},
        http: {verb: 'post'}
    });
    Task.remoteMethod('start', {
        accepts: {arg: 'id', type: 'string'},
        returns: {type: 'object', root: true},
    });
    Task.remoteMethod('addNew', {
        accepts: {arg: 'task', type: 'object', http: {source: 'body'}},
        returns: {type: 'object', root: true},
        http: {verb: 'post'}
    });
    // Task.remoteMethod('setLogins', {
    //   accepts: [{arg: 'id', type: 'string'}, {arg: 'logins', type: 'array'}],
    //   returns: {type: 'object', root: true},
    // });
    // Task.remoteMethod('resolveLogins', {
    //   accepts: {arg: 'id', type: 'string'},
    //   returns: {type: 'object', root: true},
    // });

    function joinInstagrams(newInst, oldInst) {
        newInst.forEach(function (inst) {
            if (oldInst.indexOf(inst) === -1) {
                oldInst.push(inst);
            }
        })
    }
};
