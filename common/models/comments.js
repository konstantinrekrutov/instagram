'use strict';
const Client = require('instagram-private-api').V1;
module.exports = function (Comments) {
    Comments.validatesUniquenessOf('commentId', {message: 'commentId is not unique'});
    Comments.getOldComments = function (postId, instagram, options = {}) {
        const Instagram = Comments.app.models.Instagram;

        return new Promise(function (resolve, reject) {
            Instagram.getSession(instagram).then(async function (session) {
                const feed = new Client.Feed.MediaComments(session, postId);
                // const cursor = await Comments.getCursor(postId, 'maxId');
                // if (cursor) {
                //     feed.cursorType = 'maxId';
                //     feed.cursor = cursor.cursor;
                // }
                const allResults = {};
                await Comments.app.models.Looper.interval();
                Comments.getToDate(feed, options.from, options.to, allResults, 0, function (err) {
                    if (err) {
                        return reject(err);
                    }
                    // if (feed.cursor) {
                    //     if (cursor && feed.cursor != cursor.cursor) {
                    //         cursor.cursor = feed.cursor;
                    //         cursor.save(function (err) {
                    //             if (err) {
                    //                 console.error(err);
                    //             }
                    //         })
                    //     } else if (!cursor) {
                    //         Comments.setCursor(postId, 'maxId', feed.cursor);
                    //     }
                    // }
                    resolve(allResults);
                }, function (err) {
                    console.error(err)
                });

            });
        });
    }
    Comments.getComments = function (postId, instagram, options = {}) {
        const Instagram = Comments.app.models.Instagram;

        return new Promise(function (resolve, reject) {
            Instagram.getSession(instagram).then(async function (session) {
                const feed = new Client.Feed.MediaComments(session, postId);
                const allResults = {};
                await Comments.app.models.Looper.interval();
                const cursor = await Comments.getCursor(postId, 'minId');
                // feed.cursorType = 'maxId';
                // if (cursor) {
                //     feed.cursor = cursor.cursor;
                // }
                Comments.getFromDate(feed, options.takenAt, allResults, 0, function (err) {
                    if (err) {
                        return reject(err);
                    }
                    // if (feed.cursor) {
                    //     if (cursor && feed.cursor != cursor.cursor) {
                    //         cursor.cursor = feed.cursor;
                    //         cursor.save(function (err) {
                    //             if (err) {
                    //                 console.error(err);
                    //             }
                    //         })
                    //     } else if (!cursor) {
                    //         Comments.setCursor(postId, 'minId', feed.cursor);
                    //     }
                    // }
                    resolve(allResults);
                }, function (err) {
                    console.error(err)
                });

            });
        });
    };
    Comments.setCursor = function (postId, type, cursor) {
        return new Promise(function (resolve) {
            Comments.app.models.CommentCursor.create({postId: postId, type: type, cursor: cursor}, function (err, res) {
                if (err) {
                    console.error(err);
                }
                resolve();
            })
        })

    }
    Comments.getCursor = function (postId, type) {
        return new Promise(function (resolve) {
            Comments.app.models.CommentCursor.findOne({where: {postId: postId, type: type}}, function (err, cursor) {
                resolve(cursor);
            });
        })

    }
    Comments.getToDate = function (feed, from, to, allResults, pages, cb) {
        if (pages > 15) {
            return cb(null, allResults);
        }
        pages++;
        Comments.app.models.Looper.addZapros();
        feed.get()
            .then(async function (results) {
                if (!results || !results.length) {
                    return cb(null, allResults);
                }
                results.sort((a, b) => {
                    return a.created_at - b.created_at > 0 ? 1 : -1;
                });
                for (const i in results) {
                    const item = results[i];
                    if (item.created_at > from && item.created_at < to) {
                        if (allResults[item.pk]) {
                            return cb('fff');
                        }
                        allResults[item.pk] = item;
                    } else if(item.created_at < from) {
                        return cb(null, allResults);
                    }
                }
                if (feed.moreAvailable) {
                    await Comments.app.models.Looper.interval();
                    Comments.getToDate(feed, from, to, allResults, pages, cb);
                } else {
                    return cb(null, allResults);
                }
            }, function (err) {
                return cb(err, allResults);
            })
            .catch(err => {
                console.log(err);
            });
    }
    Comments.getFromDate = function (feed, takenAt, allResults, pages, cb) {
        if (pages > 15) {
            return cb(null, allResults);
        }
        pages++;
        Comments.app.models.Looper.addZapros();
        feed.get()
            .then(async function (results) {
                if (!results || !results.length) {
                    return cb(null, allResults);
                }
                results.sort((a, b) => {
                    return a.created_at - b.created_at > 0 ? 1 : -1;
                });
                for (const i in results) {
                    const item = results[i];
                    if (item.created_at > takenAt) {
                        if (allResults[item.pk]) {
                            return cb('fff');
                        }
                        allResults[item.pk] = item;
                    } else {
                        return cb(null, allResults);
                    }
                }
                if (feed.moreAvailable) {
                    await Comments.app.models.Looper.interval();
                    Comments.getFromDate(feed, takenAt, allResults, pages, cb);
                } else {
                    return cb(null, allResults);
                }
                // return cb(null, allResults);
            }, function (err) {
                return cb(err, allResults);
            })
            .catch(err => {
                console.log(err);
            });
    };
};
