'use strict';
const Excel = require('exceljs');
module.exports = function (Report) {

    Report.makeReportFollowers = function (taskId, fromFollowers, cb) {
        const rDate = new Date();
        if (!fromFollowers) {
            fromFollowers = new Date();
        } else {
            fromFollowers = new Date(parseInt(fromFollowers));
        }
        Report.getNewFollowers(taskId, fromFollowers).then(function (followers) {
            Report.makeExcelFollowers(followers).then(function (filename) {
                Report.create({
                    taskId: taskId,
                    filename: filename,
                    date: rDate,
                    from: fromFollowers,
                    taskType: 'ParseUserTasks',
                }, function (err, report) {
                    return cb(err, report);
                });
            });
        });
    };
    Report.makeReportComments = function (taskId, fromComments, cb) {
        const rDate = new Date();

        try {
            if (!fromComments) {
                fromComments = new Date();
                fromComments.setDate(from.getDate() - 1);
            } else {
                fromComments = new Date(parseInt(fromComments));
            }

            Report.getNewComments(taskId, Math.floor(fromComments.getTime() / 1000))
                .then(function (comments) {
                    if (!comments.length) {
                        return cb({code: 'no_new_comments'});
                    }
                    Report.makeExcelComments(comments).then(function (filename) {
                        Report.create({
                            filename: filename,
                            date: rDate,
                            from: fromComments,
                            taskId: taskId,
                            taskType: 'task',
                        }, function (err, report) {
                            return cb(err, report);
                        });
                    }, function (err) {
                        cb({error: true, err: err});
                    });

                });

        } catch (e) {
            cb(e);
        }
    };

    Report.remoteMethod('makeReportComments', {
        accepts: [{type: 'string', arg: 'taskId'}, {
            type: 'string',
            arg: 'fromComments',
        }],
        returns: {type: 'object', root: true},
    });
    Report.remoteMethod('makeReportFollowers', {
        accepts: [{type: 'string', arg: 'taskId'}, {
            type: 'string',
            arg: 'fromFollowers',
        }],
        returns: {type: 'object', root: true},
    });

    Report.makeExcelFollowers = function (followers) {
        const workbook = new Excel.Workbook();
        return new Promise(function (resolve) {
            Report.count(function (err, cnt) {
                const file = 'data/f/freport' + cnt + '.xlsx';
                const worksheet = workbook.addWorksheet('comments');
                followers.forEach(function (follower) {
                    const userUrl = 'https://www.instagram.com/' + follower.username + '/';
                    worksheet.addRow([{
                        text: userUrl,
                        hyperlink: userUrl,
                        tooltip: userUrl
                    }]);
                });
                workbook.xlsx.writeFile('./client/' + file)
                    .then(function () {
                        resolve(file);
                    });
            });

        });
    };
    Report.makeExcelComments = function (comments) {
        const workbook = new Excel.Workbook();
        return new Promise(function (resolve, reject) {
            Report.count(function (err, cnt) {
                const file = 'data/c/creport' + cnt + '.xlsx';
                const worksheet = workbook.addWorksheet('comments');
                comments.forEach(function (comment) {
                    const post = comment.posts();
                    const postUrl = 'https://www.instagram.com/p/' + post['code'] + '/';
                    const userUrl = 'https://www.instagram.com/' + comment.username + '/';
                    worksheet.addRow([new Date(comment.createdAt * 1000),
                        {
                            text: postUrl,
                            hyperlink: postUrl,
                            tooltip: postUrl
                        }, {
                            text: userUrl,
                            hyperlink: userUrl,
                            tooltip: userUrl
                        }, comment.text]);
                });
                workbook.xlsx.writeFile('./client/' + file)
                    .then(function () {
                        resolve(file);
                    }, function (err) {
                        reject(err);
                    });
            });

        });

    };
    Report.getNewFollowers = async function (taskId, from) {
        let newFollowers = [];
        const task = await Report.app.models.ParseUserTasks.findById(taskId, {
            include: 'accounts',
        });
        const newFollowersIds = [];
        const troughModel = Report.app.models.followers_to_accounts;
        const newLinks = await troughModel.find({
            where: {
                parseUserTasksId: task.id,
                createdAt: {gt: from},
            },
        });
        if(!newLinks.length) {
            return [];
        }
        for (const k in newLinks) {
            if (newFollowersIds.indexOf(newLinks[k].followerId) === -1) {
                newFollowersIds.push(newLinks[k].followerId);
            }
        }

        const oldLinks = await troughModel.find({
            where: {
                id: {inq: newFollowersIds},
                parseUserTasksId: task.id,
                createdAt: {lte: from},
            },
        });
        for (const k in oldLinks) {
            const index = newFollowersIds.indexOf(oldLinks[k].followerId);
            if (index === -1) {
                newFollowersIds.splice(index, 1);
            }
        }
        if (newFollowersIds.length) {
            newFollowers = await Report.app.models.followers.find({
                where: {
                    id: {in: newFollowersIds}
                },
            });
        }
        return newFollowers;
    };
    Report.getNewComments = async function (taskId, from) {
        const newComments = [];
        const task = await Report.app.models.Task.findById(taskId, {
            include: 'accounts',
        });
        const accounts = task.accounts();
        const keywords = task.keywords.split("\n").map((item) => {
            return item.trim().toLowerCase();
        });
        const minuses = task.minus ? task.minus.split("\n").map((item) => {
            return item.trim().toLowerCase();
        }) : [];
        for (const i in accounts) {
            const account = accounts[i];
            const takenAtPosts = new Date();
            takenAtPosts.setDate(takenAtPosts.getDate() - task.daysCountPost);
            const posts = await account.posts.find({
                where: {
                    takenAt: {gt: Math.floor(takenAtPosts.getTime() / 1000)},
                },
            });
            for (const j in posts) {
                const post = posts[j];
                const comments = await post.comments.find({
                    where: {createdAt: {gt: from}},
                    include: 'posts',
                });
                comments.forEach((comment) => {
                    let finded = false;
                    let findedMinus = false;
                    const text = comment.text.toLowerCase();
                    for (const i in minuses) {
                        if (text.includes(minuses[i])) {
                            findedMinus = true;
                            return;
                        }
                    }
                    for (const i in keywords) {
                        if (text.includes(keywords[i])) {
                            finded = true;
                            break;
                        }
                    }
                    if (finded) {
                        newComments.push(comment);
                    }
                });
            }
        }
        return newComments;
    };

};
