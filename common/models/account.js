'use strict';

module.exports = function(Account) {
  Account.validatesUniquenessOf('username', {message: 'username is not unique'});

  Account.addNew = function(logins, cb) {
    const Instagram = Account.app.models.Instagram;
    let uniqueLogins = [];
    Account.find({where: {username: {inq: logins}}}, function(err, accounts) {
      const isset = accounts.map(function(item) {
        return item.username;
      });
      logins = logins.filter(function(item) {
        return isset.indexOf(item) === -1;
      });
      uniqueLogins = logins.filter(item => {
        return uniqueLogins.indexOf(item) === -1;
      });
      if (!uniqueLogins.length) {
        return cb(null, accounts);
      }
      Instagram.getUserIds(uniqueLogins).then(function(res) {
        const promises = [];
        res.forEach(function(item) {
          if (!item || !item.id) return;
          promises.push(new Promise(function(resolve, reject) {
            Account.create(item, function(err, res) {
              if (err) {
                return reject(err);
              }
              resolve(res);
            });
          }));
        });
        if (promises.length) {
          Promise.all(promises).then(function(result) {
            cb(null, result.concat(accounts));
          }, function(error) {
            cb(null, error);
          });
        } else {
          if(isset.length) {
              cb(null, accounts);
          } else {
              cb({code:'accounts_not_exists', error: true});
          }
        }
      }, function(err) {
        cb(err);
      });
    });
  };
  Account.getAccounts = async function(accounts) {
    let result;
    const splitted = await Account.splitExists(accounts);
    result = splitted['exists'];
    if(splitted['nonExists'].length) {
      const newAccounts = await Account.app.models.Instagram.getUserIds(splitted['nonExists']);
      const added = await Account.create(newAccounts);
      result = result.concat(added);
    }
    return result;
  }
  Account.splitExists = function(accounts) {
    return new Promise(function(resolve) {
      Account.find({where:{username:{inq: accounts}}}, function(err, isset) {
        if(isset && isset.length) {
          isset.forEach(item => {
            accounts.splice(accounts.indexOf(item.username), 1);
          });
        }
        resolve({nonExists: accounts, exists:isset});
      })
    });
  };
  Account.removeAll = function(filter, cb) {
    Account.destroyAll(filter,cb);
  }
  Account.remoteMethod('removeAll', {
    accepts: {arg:'filter', type: 'object'},
    returns: {type: 'object', root: true},
    http: {verb: "del"},
  });
  Account.remoteMethod('addNew', {
    accepts: {arg: 'logins', type: 'array'},
    returns: {type: 'object', root: true},
  });
};
