'use strict';
module.exports = function(Parseusertasks) {
  Parseusertasks.validatesUniquenessOf('name', {message: 'name is not unique'});

  Parseusertasks.addNew = function(task, cb) {
    let accounts = task['accounts'].split("\n").map(item => {
      return item.trim();
    });
    const instagram = task['instagrams'];
    delete task['accounts'];
    delete task['instagrams'];
    Parseusertasks.create(task, async function(err, task) {
      if(err) { return cb(err); }
      const taskId = task.id.toString();
      accounts = await Parseusertasks.app.models.Account.getAccounts(accounts);
      await Parseusertasks.app.models.parseuser_to_accounts.create(
        accounts.map(item => {
          return {taskId: taskId, accountId: item.id};
        }));
      await Parseusertasks.app.models.parseuser_to_instagram.create(
        instagram.map(item => {
          return {taskId: taskId, instagramId: item};
        }));
      cb(null, task);
    })
  };
  Parseusertasks.editTask = function (body, cb) {
    if (!body.task.id) {
      return cb({code: 'id_is_required', error: true})
    }
    const newTask = body.task;
    let accounts = body.accounts.split("\n").map(item => {
      return item.trim();
    });
    const instagrams = body.instagrams;
    const oldAccs = newTask.accounts.map(function (acc) {
      return acc.username;
    });
    const oldAccounts = newTask.accounts;
    delete newTask.accounts;
    Parseusertasks.findById(newTask.id, function (err, task) {
      if (err || !task) {
        return cb({code: 'server_error', error: true});
      }
      const promises = [];
      if(!instagrams.equals(newTask.instagrams)) {
        promises.push(new Promise(function (resolve, reject) {
          task.instagrams.destroyAll(function (err) {
            if (err) {
              return reject(err);
            }
            const troughModel = Parseusertasks.app.models.parseuser_to_instagram;
            troughModel.create(instagrams.map(function (item) {
              return {taskId: task.id, instagramId: item};
            }), function (err, instas) {
              if (err) {
                return reject(err);
              }
              resolve(instas);

            })
          });
        }))
      }

      if (!accounts.equals(oldAccs)) {
        promises.push(new Promise(async function(resolve, reject) {
          accounts = await Parseusertasks.app.models.Account.getAccounts(accounts);
          task.accounts.destroyAll(function(err) {
            if (err) {
              return reject(err);
            }
            const troughModel = Parseusertasks.app.models.parseuser_to_accounts;
            troughModel.create(accounts.map(function(item) {
              return {taskId: task.id, accountId: item.id};
            }), function(err, addedAccounts) {
              if (err) {
                return reject(err);
              }
              resolve(addedAccounts);

            });
          });
        }))
      }
      promises.push(new Promise(function (resolve, reject) {
        task.updateAttributes(newTask, function (err, res) {
          if (err) {
            return reject(err);
          }
          resolve(res);
        })
      }));
      Promise.all(promises).then(function (res) {
        cb(null, {code: 'task_saved'});
      }, function (err) {
        cb(err);
      })
    });

  };

  Parseusertasks.removeAll = function(filter, cb) {
    Parseusertasks.destroyAll(filter, cb);
  };
  Parseusertasks.remoteMethod('removeAll', {
    accepts: {arg:'filter', type: 'object'},
    returns: {type: 'object', root: true},
    http: {verb: "del"},
  });
  Parseusertasks.remoteMethod('addNew', {
    accepts: {arg:'task', type:'object', http: {source: 'body'}},
    returns: {type: 'object', root: true}
  });
  Parseusertasks.remoteMethod('editTask', {
    accepts: {arg:'task', type:'object', http: {source: 'body'}},
    returns: {type: 'object', root: true}
  });
};
